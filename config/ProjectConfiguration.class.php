<?php

require_once dirname(__FILE__) . '/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('sfDoctrinePlugin');
    $this->enablePlugins('sfDoctrineGuardPlugin');
    $this->enablePlugins('sfForkedDoctrineApplyPlugin');
    $this->enablePlugins('dvTwitterBootstrapPlugin');
    $this->enablePlugins('sfFormExtraPlugin');
    //$this->enablePlugins('sfMyMasterPaypalPlugin');
    //$this->enablePlugins('majaxJqueryPlugin');
    $this->enablePlugins('majaxMarkdownPlugin');
    $this->enablePlugins('sfErrorNotifierPlugin');

  }
}

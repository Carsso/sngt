set :application, "SNGT"
set :domain,      "zam.wanadev.net"
set :deploy_to,   "/var/www/clients/client2/web1/web"

set :repository,  "git@bitbucket.org:Carsso/sngt.git"
set :scm,         :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`
set :deploy_via, :copy

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain                         # This may be the same as your `Web` server
role :db,         domain, :primary => true       # This is where symfony migrations will run

set :keep_releases,  5
set :user,       "web1"
set :shared_files,      ["config/databases.yml", "web/sngt_back_dev.php", "web/sngt_front_dev.php", "web/sngt_entry_dev.php"]
set :shared_children,     ["web/uploads", "lib/vendor"]
set :use_sudo,    false
set :group_writable, true
set :clear_controllers, false
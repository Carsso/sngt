<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<vxml xmlns="http://www.w3.org/2001/vxml" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.w3.org/2001/vxml 
   http://www.w3.org/TR/voicexml20/vxml.xsd"
   version="2.0">    
     
<property name="inputmodes" value="dtmf"/> 

    <form id="mainMenu">
        <block>
            <prompt bargein="true" xml:lang="fr">
            

<?php
    // Verification des horaires d'ouvertures
    // (de 8h à 20h)
    $ouvert = false;
        
    if(date("G") >= 8 && date("G") <= 19){ 
        $ouvert = true;
    }
    // Service ouvert, Debut script VoiceXML
    if($ouvert){
?>
                <audio src="http://sngt.fr/pabx/sons/bienvenue.wav">Bienvenue sur le serveur vocal de la LAN SOPA. Merci de bien vouloir patienter quelques instants, je vous met en relation avec un administrateur.</audio>
            </prompt>
            <goto next="#sngt_go" />
<?php
    // Service ferme, Debut script VoiceXML
    } else {
?>
                <audio src="http://sngt.fr/pabx/sons/ferme.wav">Bienvenue sur le serveur vocal de la LAN SOPA. Vous pouvez nous contacter uniquement de 8h à 20h. Nous ne sommes pas disponibles actuellement, merci de renouveller votre appel ultérieurement.</audio>
            </prompt>
<?php
    }
?>
        </block>

    </form>
    
    <form id="sngt_go">
        <transfer name="bridgeTransfer" bridge="true" 
                  destexpr="'number/0972339767'" connecttimeout="120s" >
            <filled>
                <audio src="http://sngt.fr/pabx/sons/indisponible.wav">Désolé, aucun des aministrateurs n'est actuellement disponible pour vous répondre. Merci de renouveller votre appel ultérieurement.</audio>
            </filled>
        </transfer>
    </form>

</vxml>

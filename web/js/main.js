var debug = false;
function logMe(value){
  if(debug){
    console.log(value);
  }
}

function equalHeight(group) {
  var tallest = 0;
  group.each(function() {
    var thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.css('min-height',tallest);
}

$(document).ready(function() {
  $("textarea").autoresize();
  equalHeight($(".column"));
  $('[data-toggle=collapse]').click(function(){
    equalHeight($(".column"));
  });
});

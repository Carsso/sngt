<?php

/**
 * Step form base class.
 *
 * @method Step getObject() Returns the current form's model object
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseStepForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'city'             => new sfWidgetFormInputText(),
      'content'          => new sfWidgetFormInputText(),
      'content_prizes'   => new sfWidgetFormInputText(),
      'content_schedule' => new sfWidgetFormInputText(),
      'content_hotel'    => new sfWidgetFormInputText(),
      'content_access'   => new sfWidgetFormInputText(),
      'content_map'      => new sfWidgetFormInputText(),
      'price'            => new sfWidgetFormInputText(),
      'image'            => new sfWidgetFormInputText(),
      'status'           => new sfWidgetFormInputCheckbox(),
      'is_giga'          => new sfWidgetFormInputCheckbox(),
      'display_on_list'  => new sfWidgetFormInputCheckbox(),
      'start_date'       => new sfWidgetFormInputText(),
      'end_date'         => new sfWidgetFormInputText(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'slug'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'city'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'content'          => new sfValidatorPass(array('required' => false)),
      'content_prizes'   => new sfValidatorPass(array('required' => false)),
      'content_schedule' => new sfValidatorPass(array('required' => false)),
      'content_hotel'    => new sfValidatorPass(array('required' => false)),
      'content_access'   => new sfValidatorPass(array('required' => false)),
      'content_map'      => new sfValidatorPass(array('required' => false)),
      'price'            => new sfValidatorNumber(array('required' => false)),
      'image'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'status'           => new sfValidatorBoolean(array('required' => false)),
      'is_giga'          => new sfValidatorBoolean(array('required' => false)),
      'display_on_list'  => new sfValidatorBoolean(array('required' => false)),
      'start_date'       => new sfValidatorPass(array('required' => false)),
      'end_date'         => new sfValidatorPass(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'slug'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Step', 'column' => array('slug')))
    );

    $this->widgetSchema->setNameFormat('step[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Step';
  }

}

<?php

/**
 * sfGuardUserProfile form base class.
 *
 * @method sfGuardUserProfile getObject() Returns the current form's model object
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'user_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => false)),
      'email_new'                => new sfWidgetFormInputText(),
      'firstname'                => new sfWidgetFormInputText(),
      'lastname'                 => new sfWidgetFormInputText(),
      'validate_at'              => new sfWidgetFormDateTime(),
      'validate'                 => new sfWidgetFormInputText(),
      'type'                     => new sfWidgetFormInputText(),
      'birthday'                 => new sfWidgetFormDate(),
      'phone'                    => new sfWidgetFormInputText(),
      'address'                  => new sfWidgetFormInputText(),
      'postal_code'              => new sfWidgetFormInputText(),
      'city'                     => new sfWidgetFormInputText(),
      'country'                  => new sfWidgetFormInputText(),
      'masters_license'          => new sfWidgetFormInputText(),
      'identity_card_type'       => new sfWidgetFormInputText(),
      'identity_card_type_other' => new sfWidgetFormInputText(),
      'identity_card_delivery'   => new sfWidgetFormDate(),
      'identity_card'            => new sfWidgetFormInputText(),
      'entry_code'               => new sfWidgetFormInputText(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'))),
      'email_new'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'firstname'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'lastname'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'validate_at'              => new sfValidatorDateTime(array('required' => false)),
      'validate'                 => new sfValidatorString(array('max_length' => 33, 'required' => false)),
      'type'                     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'birthday'                 => new sfValidatorDate(array('required' => false)),
      'phone'                    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address'                  => new sfValidatorPass(array('required' => false)),
      'postal_code'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'                     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'country'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'masters_license'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'identity_card_type'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'identity_card_type_other' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'identity_card_delivery'   => new sfValidatorDate(array('required' => false)),
      'identity_card'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'entry_code'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUserProfile', 'column' => array('user_id'))),
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUserProfile', 'column' => array('email_new'))),
      ))
    );

    $this->widgetSchema->setNameFormat('sf_guard_user_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

}

<?php

/**
 * Profile form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProfileForm extends BaseProfileForm
{
  /**
   * @see sfGuardUserProfileForm
   */
  public function configure()
  {
    parent::configure();
  }
}

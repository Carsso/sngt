<?php

/**
 * sfGuardPermission form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardPermissionForm extends PluginsfGuardPermissionForm
{
  public function configure()
  {
    $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array( 'expanded' => true, 'multiple' => true, 'model' => 'sfGuardGroup', 'renderer_class' => 'sfWidgetFormSelectCheckboxBootstrap'));
    $this->widgetSchema['users_list'] = new sfWidgetFormDoctrineChoice(array( 'expanded' => true, 'multiple' => true, 'model' => 'sfGuardUser', 'renderer_class' => 'sfWidgetFormSelectCheckboxBootstrap'));
    unset($this['created_at'], $this['updated_at'], $this['slug']);
    $decorator = new sfWidgetFormSchemaFormatterBootstrap($this->widgetSchema);
    $this->widgetSchema->addFormFormatter('custom', $decorator);
    $this->widgetSchema->setFormFormatterName('custom');
  }
}

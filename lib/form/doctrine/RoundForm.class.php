<?php

/**
 * Round form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class RoundForm extends BaseRoundForm
{
  public function configure()
  {
    $this->widgetSchema['yurplan_event_id'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge'));
    unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}

<?php

/**
 * TopNews form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TopNewsForm extends BaseTopNewsForm
{
  public function configure()
  {
    $this->widgetSchema['image'] = new sfWidgetFormSelect(array('choices' => misc::getImagesGames()));
    unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}

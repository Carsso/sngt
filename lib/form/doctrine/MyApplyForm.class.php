<?php

/**
 * Cms form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MyApplyForm extends sfApplyApplyForm
{
  public function configure()
  {
    parent::configure();
    $this->widgetSchema['username'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['email'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required', 'type' => 'email'));
    $this->widgetSchema['email2'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required', 'type' => 'email'));
    $this->widgetSchema['password'] = new sfWidgetFormInputPassword(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['password2'] = new sfWidgetFormInputPassword(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['lastname'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['firstname'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $years = range(date('Y') - 80, date('Y') - 10);
    $this->widgetSchema['birthday'] = new sfWidgetFormDate(array('format' => '%day%/%month%/%year%', 'years' => array_combine($years, $years)), array('class' => 'input-medium', 'required' => 'required'));
    $this->widgetSchema['phone'] = new sfWidgetFormInputText(array(), array('placeholder' => 'Ex: 0612345678 pour un téléphone français ou +441234567890 pour un non-français', 'class' => 'input-xxlarge', 'required' => 'required', 'pattern'=>'^((\+([0-9]+))|(0|\+33)[6-7]([0-9]{8}))$'));
    $this->widgetSchema['address'] = new sfWidgetFormTextarea(array(), array('class' => 'input-xxlarge', 'required' => 'required', 'rows'=>2));
    $this->widgetSchema['postal_code'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['city'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['country'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $pi_type = array(
      'cni' => 'Carte d\'identité Française',
      'passeport' => 'Passeport',
      'permis' => 'Permis de conduire Français',
      'other' => 'Autre',
      'no' => 'Je ne souhaite pas fournir cette information'
    );
    $this->widgetSchema['identity_card_type'] = new sfWidgetFormSelect(array('choices' => $pi_type), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['identity_card_type_other'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge'));
    $this->widgetSchema['identity_card_authority'] = new sfWidgetFormInputText(array(), array('placeholder' => 'Ex: Préfecture de la Côte d\'Or, Sous-préfecture de Mulhouse', 'class' => 'input-xxlarge', 'required' => 'required'));
    $years = range(date('Y') - 30, date('Y'));
    $this->widgetSchema['identity_card_delivery'] = new sfWidgetFormDate(array('format' => '%day%/%month%/%year%', 'years' => array_combine($years, $years)), array('class' => 'input-medium', 'required' => 'required'));
    $this->widgetSchema['identity_card'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge', 'required' => 'required'));
    $this->widgetSchema['masters_license'] = new sfWidgetFormInputText(array(), array('placeholder' => 'N°de votre license (si vous avez une license des Masters Français du Jeu Vidéo)', 'class' => 'input-xxlarge'));
    $this->widgetSchema->setLabels(array(
      'username' => 'Pseudo *',
      'email' => 'Adresse e-mail *',
      'email2' => 'Adresse e-mail (vérification) *',
      'password' => 'Mot de passe *',
      'password2' => 'Mot de passe (vérification) *',
      'lastname' => 'Nom *',
      'firstname' => 'Prénom *',
      'birthday' => 'Date de naissance *',
      'phone' => 'Numéro de téléphone portable *',
      'address' => 'Adresse Postale *',
      'postal_code' => 'Code Postal *',
      'city' => 'Ville *',
      'country' => 'Pays *',
      'identity_card_type' => 'Nature de la pièce d\'identité *',
      'identity_card_type_other' => 'Nature de la pièce d\'identité (détail) *',
      'identity_card_delivery' => 'Date de délivrance de la pièce d\'identité *',
      'identity_card_authority' => 'Autorité de délivrance de la pièce d\'identité *',
      'identity_card' => 'N° de la pièce d\'identité *',
      'masters_license' => 'N°de la license Masters',
    ));
    $this->validatorSchema['firstname'] = new sfValidatorString(array('max_length' => 255, 'required' => true));
    $this->validatorSchema['lastname'] = new sfValidatorString(array('max_length' => 255, 'required' => true));
    $this->validatorSchema['birthday'] = new sfValidatorDate(array('required' => true));
    $this->validatorSchema['phone'] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^((\+([0-9]+))|(0|\+33)[6-7]([0-9]{8}))$/'));
    $this->validatorSchema['address'] = new sfValidatorString(array('required' => true));
    $this->validatorSchema['postal_code'] = new sfValidatorString(array('max_length' => 255, 'required' => true));
    $this->validatorSchema['city'] = new sfValidatorString(array('max_length' => 255, 'required' => true));
    $this->validatorSchema['country'] = new sfValidatorString(array('max_length' => 255, 'required' => true));
    $this->validatorSchema['identity_card_type'] = new sfValidatorString(array('max_length' => 255, 'required' => true));
    $this->validatorSchema['identity_card_type_other'] = new sfValidatorString(array('max_length' => 255, 'required' => false));
    $this->validatorSchema['identity_card_delivery'] = new sfValidatorDate(array('required' => false));
    $this->validatorSchema['identity_card_authority'] = new sfValidatorString(array('max_length' => 255, 'required' => false));
    $this->validatorSchema['identity_card'] = new sfValidatorString(array('max_length' => 255, 'required' => false));
    $decorator = new sfWidgetFormSchemaFormatterBootstrap($this->widgetSchema);
    $this->widgetSchema->addFormFormatter('custom', $decorator);
    $this->widgetSchema->setFormFormatterName('custom');
  }
}

<?php

/**
 * sfGuardUserAdminForm for admin generators
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUserAdminForm.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class sfGuardUserAdminForm extends BasesfGuardUserAdminForm
{
  /**
   * @see sfForm
   */
  public function configure()
  {
    parent::configure();
    $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array( 'expanded' => true, 'multiple' => true, 'model' => 'sfGuardGroup', 'renderer_class' => 'sfWidgetFormSelectCheckboxBootstrap'));
    $this->widgetSchema['is_super_admin'] = new sfWidgetFormInputHidden();
    unset($this['first_name'],$this['last_name']);
  }
}

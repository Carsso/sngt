<?php

/**
 * Step form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class StepForm extends BaseStepForm
{
  public function configure()
  {
    $this->widgetSchema['image'] = new sfWidgetFormSelect(array('choices' => array_merge(array('null'=>''),misc::getImagesSteps())));
    $this->widgetSchema['city'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge'));
    $this->widgetSchema['status'] = new sfWidgetFormSelect(array('choices' => array(0 => 'Réservation fermée', 1 => 'Ouvert à la réservation')), array('class' => 'input-xxlarge'));
    $this->widgetSchema['price'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge'));
    $this->widgetSchema['display_on_list'] = new sfWidgetFormSelect(array('choices' => array(0 => 'NE PAS afficher sur la page "Etapes"', 1 => 'Afficher sur la page "Etapes"')), array('class' => 'input-xxlarge'));
    $this->widgetSchema['content'] = new majaxWidgetFormMarkdownEditor(array(), array('class' => 'input-xxlarge', 'cols' => 30, 'rows' => 10, 'required' => 'required', 'style' => 'height: 210px;'));
    $this->widgetSchema['content_schedule'] = new majaxWidgetFormMarkdownEditor(array(), array('class' => 'input-xxlarge', 'cols' => 30, 'rows' => 10, 'required' => 'required', 'style' => 'height: 210px;'));
    $this->widgetSchema['content_prizes'] = new majaxWidgetFormMarkdownEditor(array(), array('class' => 'input-xxlarge', 'cols' => 30, 'rows' => 10, 'required' => 'required', 'style' => 'height: 210px;'));
    $this->widgetSchema['content_hotel'] = new majaxWidgetFormMarkdownEditor(array(), array('class' => 'input-xxlarge', 'cols' => 30, 'rows' => 10, 'required' => 'required', 'style' => 'height: 210px;'));
    $this->widgetSchema['content_access'] = new majaxWidgetFormMarkdownEditor(array(), array('class' => 'input-xxlarge', 'cols' => 30, 'rows' => 10, 'required' => 'required', 'style' => 'height: 210px;'));
    $this->widgetSchema['content_map'] = new sfWidgetFormTextarea(array(), array('class' => 'input-xxlarge'));
    unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}

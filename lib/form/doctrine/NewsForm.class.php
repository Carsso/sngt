<?php

/**
 * News form.
 *
 * @package    SNGT
 * @subpackage form
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsForm extends BaseNewsForm
{
  public function configure()
  {
    $this->widgetSchema['title'] = new sfWidgetFormInputText(array(), array('class' => 'input-xxlarge'));
    $this->widgetSchema['content'] = new majaxWidgetFormMarkdownEditor(array(), array('class' => 'input-xxlarge', 'cols' => 30, 'rows' => 10, 'required' => 'required', 'style' => 'height: 210px;'));
    unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}

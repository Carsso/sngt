<?php

/**
 * Game
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    SNGT
 * @subpackage model
 * @author     Carsso
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Game extends BaseGame
{
  public function __toString()
  {
    return $this->getName() . ' (' . $this->getFormattedPlayers().')';
  }

  public function getFormattedPlayers()
  {
    if ($this->getTeamNbPlayers() == 1):
      return $this->getMaxNbTeams().' joueurs en solo';
    else:
      return $this->getMaxNbTeams().' équipes de '.$this->getTeamNbPlayers().' joueurs';
    endif;
  }

  public function getLittleName()
  {
    $result = explode(' - ', $this->getName());
    return $result[0];
  }
}

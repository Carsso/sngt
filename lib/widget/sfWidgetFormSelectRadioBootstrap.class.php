<?php

class sfWidgetFormSelectRadioBootstrap extends sfWidgetFormSelectRadio {
  protected function formatChoices($name, $value, $choices, $attributes)
  {
    $inputs = array();
    foreach ($choices as $key => $option)
    {
      $baseAttributes = array(
        'name'  => substr($name, 0, -2),
        'type'  => 'radio',
        'value' => self::escapeOnce($key),
        'id'    => $id = $this->generateId($name, self::escapeOnce($key)),
      );

      if (strval($key) == strval($value === false ? 0 : $value))
      {
        $baseAttributes['checked'] = 'checked';
      }

      $input_rendered = $this->renderTag('input', array_merge($baseAttributes, $attributes));
      $inputs[$id] = $this->renderContentTag('label', $input_rendered.$this->getOption('separator').self::escapeOnce($option), array('for' => $id));
    }

    return call_user_func($this->getOption('formatter'), $this, $inputs);
  }

  public function formatter($widget, $inputs)
  {
    $rows = array();
    foreach ($inputs as $input)
    {
      $rows[] = $input;
    }
    return implode($this->getOption('separator'), $rows);
  }
}
<?php

class sfWidgetFormSelectCheckboxBootstrap extends sfWidgetFormSelectCheckbox {
  protected function formatChoices($name, $value, $choices, $attributes)
  {
    $inputs = array();
    foreach ($choices as $key => $option)
    {
      $baseAttributes = array(
        'name'  => $name,
        'type'  => 'checkbox',
        'value' => self::escapeOnce($key),
        'id'    => $id = $this->generateId($name, self::escapeOnce($key)),
      );

      if ((is_array($value) && in_array(strval($key), $value)) || (is_string($value) && strval($key) == strval($value)))
      {
        $baseAttributes['checked'] = 'checked';
      }

      $input_rendered = $this->renderTag('input', array_merge($baseAttributes, $attributes));
      $inputs[$id] = $this->renderContentTag('label', $input_rendered.$this->getOption('separator').self::escapeOnce($option), array('for' => $id));
    }

    return call_user_func($this->getOption('formatter'), $this, $inputs);
  }
  public function formatter($widget, $inputs)
  {
    $rows = array();
    foreach ($inputs as $input)
    {
      $rows[] = $input;
    }
    return implode($this->getOption('separator'), $rows);
  }
}
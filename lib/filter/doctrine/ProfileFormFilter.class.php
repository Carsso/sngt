<?php

/**
 * Profile filter form.
 *
 * @package    SNGT
 * @subpackage filter
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProfileFormFilter extends BaseProfileFormFilter
{
  /**
   * @see sfGuardUserProfileFormFilter
   */
  public function configure()
  {
    parent::configure();
  }
}

<?php

/**
 * Step filter form base class.
 *
 * @package    SNGT
 * @subpackage filter
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseStepFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'city'             => new sfWidgetFormFilterInput(),
      'content'          => new sfWidgetFormFilterInput(),
      'content_prizes'   => new sfWidgetFormFilterInput(),
      'content_schedule' => new sfWidgetFormFilterInput(),
      'content_hotel'    => new sfWidgetFormFilterInput(),
      'content_access'   => new sfWidgetFormFilterInput(),
      'content_map'      => new sfWidgetFormFilterInput(),
      'price'            => new sfWidgetFormFilterInput(),
      'image'            => new sfWidgetFormFilterInput(),
      'status'           => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_giga'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'display_on_list'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'start_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'end_date'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'slug'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'city'             => new sfValidatorPass(array('required' => false)),
      'content'          => new sfValidatorPass(array('required' => false)),
      'content_prizes'   => new sfValidatorPass(array('required' => false)),
      'content_schedule' => new sfValidatorPass(array('required' => false)),
      'content_hotel'    => new sfValidatorPass(array('required' => false)),
      'content_access'   => new sfValidatorPass(array('required' => false)),
      'content_map'      => new sfValidatorPass(array('required' => false)),
      'price'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'image'            => new sfValidatorPass(array('required' => false)),
      'status'           => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_giga'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'display_on_list'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'start_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'end_date'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'slug'             => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('step_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Step';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'city'             => 'Text',
      'content'          => 'Text',
      'content_prizes'   => 'Text',
      'content_schedule' => 'Text',
      'content_hotel'    => 'Text',
      'content_access'   => 'Text',
      'content_map'      => 'Text',
      'price'            => 'Number',
      'image'            => 'Text',
      'status'           => 'Boolean',
      'is_giga'          => 'Boolean',
      'display_on_list'  => 'Boolean',
      'start_date'       => 'Date',
      'end_date'         => 'Date',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'slug'             => 'Text',
    );
  }
}

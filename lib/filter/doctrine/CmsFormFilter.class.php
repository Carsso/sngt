<?php

/**
 * Cms filter form.
 *
 * @package    SNGT
 * @subpackage filter
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsFormFilter extends BaseCmsFormFilter
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at'], $this['slug']);
  }
}

<?php

/**
 * sfGuardUserProfile filter form base class.
 *
 * @package    SNGT
 * @subpackage filter
 * @author     Carsso
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'email_new'                => new sfWidgetFormFilterInput(),
      'firstname'                => new sfWidgetFormFilterInput(),
      'lastname'                 => new sfWidgetFormFilterInput(),
      'validate_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'validate'                 => new sfWidgetFormFilterInput(),
      'type'                     => new sfWidgetFormFilterInput(),
      'birthday'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'phone'                    => new sfWidgetFormFilterInput(),
      'address'                  => new sfWidgetFormFilterInput(),
      'postal_code'              => new sfWidgetFormFilterInput(),
      'city'                     => new sfWidgetFormFilterInput(),
      'country'                  => new sfWidgetFormFilterInput(),
      'masters_license'          => new sfWidgetFormFilterInput(),
      'identity_card_type'       => new sfWidgetFormFilterInput(),
      'identity_card_type_other' => new sfWidgetFormFilterInput(),
      'identity_card_delivery'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'identity_card'            => new sfWidgetFormFilterInput(),
      'entry_code'               => new sfWidgetFormFilterInput(),
      'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'id')),
      'email_new'                => new sfValidatorPass(array('required' => false)),
      'firstname'                => new sfValidatorPass(array('required' => false)),
      'lastname'                 => new sfValidatorPass(array('required' => false)),
      'validate_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'validate'                 => new sfValidatorPass(array('required' => false)),
      'type'                     => new sfValidatorPass(array('required' => false)),
      'birthday'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'phone'                    => new sfValidatorPass(array('required' => false)),
      'address'                  => new sfValidatorPass(array('required' => false)),
      'postal_code'              => new sfValidatorPass(array('required' => false)),
      'city'                     => new sfValidatorPass(array('required' => false)),
      'country'                  => new sfValidatorPass(array('required' => false)),
      'masters_license'          => new sfValidatorPass(array('required' => false)),
      'identity_card_type'       => new sfValidatorPass(array('required' => false)),
      'identity_card_type_other' => new sfValidatorPass(array('required' => false)),
      'identity_card_delivery'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'identity_card'            => new sfValidatorPass(array('required' => false)),
      'entry_code'               => new sfValidatorPass(array('required' => false)),
      'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'user_id'                  => 'ForeignKey',
      'email_new'                => 'Text',
      'firstname'                => 'Text',
      'lastname'                 => 'Text',
      'validate_at'              => 'Date',
      'validate'                 => 'Text',
      'type'                     => 'Text',
      'birthday'                 => 'Date',
      'phone'                    => 'Text',
      'address'                  => 'Text',
      'postal_code'              => 'Text',
      'city'                     => 'Text',
      'country'                  => 'Text',
      'masters_license'          => 'Text',
      'identity_card_type'       => 'Text',
      'identity_card_type_other' => 'Text',
      'identity_card_delivery'   => 'Date',
      'identity_card'            => 'Text',
      'entry_code'               => 'Text',
      'created_at'               => 'Date',
      'updated_at'               => 'Date',
    );
  }
}

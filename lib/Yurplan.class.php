<?php
class Yurplan
{
  public $token;
  public $yurplanurl;
  public $yurplankey;
  public $yurplanuser;
  public $yurplanpwd;

  public function __construct()
  {
    $this->yurplanurl = sfConfig::get('app_yurplan_url');
    $this->yurplankey = sfConfig::get('app_yurplan_key');
    $this->yurplanuser = sfConfig::get('app_yurplan_user');
    $this->yurplanpwd = sfConfig::get('app_yurplan_pwd');
  }

  public function login()
  {
    $json_result = misc::getWithCURL($this->yurplanurl . '/api.php/auth?key=' . $this->yurplankey, array('email' => $this->yurplanuser, 'password' => $this->yurplanpwd), true);
    $result = json_decode($json_result);
    $this->token = $result->data->token;
  }

  public function checkTicket($yurplanid, $search)
  {
    if (!$this->token) {
      $this->login();
    }
    $json_result = misc::getWithCURL($this->yurplanurl . '/api.php/events/' . $yurplanid . '/tickets/find?key=' . $this->yurplankey . '&token=' . $this->token . '&search=' . urlencode($search));
    return $json_result;
  }

  public function listTickets($yurplanid)
  {
    if (!$this->token) {
      $this->login();
    }
    if (!$yurplanid)
      return null;
    $json_result = misc::getWithCURL($this->yurplanurl . '/api.php/events/' . $yurplanid . '/tickets?key=' . $this->yurplankey . '&token=' . $this->token);
    return $json_result;
  }

  public function updatePayment($yurplanid, $player)
  {
    if ($yurplanid) {
      $json_tickets = $this->checkTicket($yurplanid, $player->getSfGuardUser()->getEmailAddress());
      $tickets = json_decode($json_tickets);
      if ($tickets->data->count) {
        $player->setIsPayed(true)
            ->setYurplanToken($tickets->data->tickets[0]->token)
            ->setPaymentDate(date('Y-m-d H:i:s', $tickets->data->tickets[0]->buy_date))
            ->setTransaction($json_tickets)
            ->save();
      }
    }
  }

}

?>

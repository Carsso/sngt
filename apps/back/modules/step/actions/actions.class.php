<?php

require_once dirname(__FILE__) . '/../lib/stepGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/stepGeneratorHelper.class.php';

/**
 * step actions.
 *
 * @package    SNGT
 * @subpackage step
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class stepActions extends autoStepActions
{
}

<?php

require_once dirname(__FILE__) . '/../lib/playerGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/playerGeneratorHelper.class.php';

/**
 * player actions.
 *
 * @package    SNGT
 * @subpackage player
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class playerActions extends autoPlayerActions
{
}

<?php

/**
 * recap actions.
 *
 * @package    SNGT
 * @subpackage recap
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class recapActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->steps = Doctrine::getTable('Step')->findAll();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->step = $this->getRoute()->getObject();
  }

  public function executeMail(sfWebRequest $request)
  {
    $steps = Doctrine::getTable('Step')->findAll();
    foreach ($steps as $step) {
      foreach ($step->getRound() as $round) {
        foreach ($round->getTeam() as $team) {
          foreach ($team->getPlayer() as $player) {
            $user = $player->getUser();
            if (filter_var($user->getEmailAddress(), FILTER_VALIDATE_EMAIL))
              $spam[$user->getEmailAddress()] = 1;
          }
        }
      }
    }
    foreach ($spam as $email => $spamming) {
      echo $email . ', ';
    }
    die();
  }

  public function executeMailstep(sfWebRequest $request)
  {
    $spam = array();
    $step = $this->getRoute()->getObject();
    foreach ($step->getRound() as $round) {
      foreach ($round->getTeam() as $team) {
        foreach ($team->getPlayer() as $player) {
          $user = $player->getUser();
          if (filter_var($user->getEmailAddress(), FILTER_VALIDATE_EMAIL))
            $spam[$user->getEmailAddress()] = 1;
        }
      }
    }
    foreach ($spam as $email => $spamming) {
      echo $email . ', ';
    }
    die();
  }

  public function executeCsv(sfWebRequest $request)
  {
    $step = $this->getRoute()->getObject();
    $out = '';
    foreach ($step->getRound() as $round) {
      $out .= '"Round";"' . $round->__toString() . '"';
      $out .= "\r\n";
      $out .= "\r\n";
      foreach ($round->getTeam() as $team) {
        if ($team->getSlotReserved()) {
          $out .= '"";"Equipe";"' . $team->__toString() . '"';
          $out .= "\r\n";
          $out .= '"";"Pseudo";"Email";"Téléphone";"Prénom";"Nom";"Nuit Vendredi-Samedi";"Nuit Samedi-Dimanche";"Payé"';
          $out .= "\r\n";
          foreach ($team->getPlayer() as $player) {
            $payment = $player->getPayment();
            $out .= '"";"' . $player->getPseudo() . '";"' . $player->getEmail() . '";"' . $player->getPhone() . '";"' . $player->getFirstName() . '";"' . $player->getLastName() . '";"' . (($player->getNight1()) ? 'Oui' : 'Non') . '";"' . (($player->getNight2()) ? 'Oui' : 'Non') . '";"' . (($payment->getPayed()) ? '20€' : '') . '"';
            $out .= "\r\n";
          }
          $out .= "\r\n";
        }
      }
      $out .= "\r\n\r\n";
    }
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    $response->setStatusCode(200);
    $response->setContentType('application/vnd.ms-excel');
    $response->setHttpHeader('Expires', $this->getContext()->getResponse()->getDate(time()), true);
    $response->setHttpHeader('Last-Modified', gmdate('D, d M Y H:i:s') . ' GMT', true);
    $response->setHttpHeader('Cache-Control', 'no-cache', true);
    $response->sendHttpHeaders();
    $response->setContent(utf8_decode($out));
    return sfView::NONE;
  }
}

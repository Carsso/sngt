<div style="margin-top: 20px;">
  <?php foreach ($step->getRound() as $round): ?>
    <h3>
      <?php echo $round->__toString() ?><br/>
      <small>
        <?php echo $round->getNbTeamReserved() . '/' . $round->getGame()->getMaxNbTeams() ?> Equipes réservées -
        <?php echo $round->getNbTeamFree() ?> Places restantes
      </small>
    </h3>
    <div>
      <?php foreach ($round->getTeam() as $team): ?>
        <div style="margin-left: 40px; margin-bottom:10px;<?php echo (($team->getIsReserved()) ? 'color:green' : 'color:red') ?>">
          Equipe: <?php echo $team ?> - Id: <?php echo $team->getId() ?> -
          Réservée: <?php echo ($team->getIsReserved() ? 'Oui' : 'Non') ?></strong><br/>
          <?php foreach ($team->getPlayer() as $player): ?>
            <?php $color = (($player->getIsPayed()) ? 'green' : (($team->getIsReserved()) ? 'orange' : 'red')) ?>

            <div style="margin-left: 40px;color:<?php echo $color ?>">
              Joueur: <?php echo $player->getUserName() ?> - Id: <?php echo $player->getId() ?> -
              UserID: <?php echo $player->getUser()->getId() ?> -
              Email: <?php echo $player->getUser()->getEmailAddress()  ?>

              <div style="margin-left: 40px;color:<?php echo $color ?>">
                Prénom: <?php echo $player->getProfile()->getFirstname() ?> -
                Nom: <?php echo $player->getProfile()->getLastname() ?><br/>
                Téléphone: <?php echo $player->getProfile()->getPhone() ?><br/>
                Payé: <?php echo ($player->getIsPayed() ? 'oui' : 'non') ?> -
                Date: <?php echo $player->getPaymentDate() ?><br/>
                Transaction: <?php echo $player->getTransaction() ?>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endforeach; ?>
</div>
<div style="margin-top: 20px;text-align: center;">
  <?php foreach ($steps as $step): ?>
    <div class="boxed text_center">
      <p>
        <?php echo link_to('Récapitulatif de l\'étape ' . $step, 'recaps', $step); ?>
        <?php echo ' (' . link_to('CSV', 'recaps_csv', $step) . ')'; ?>
      </p>
    </div>
  <?php endforeach; ?>
</div>
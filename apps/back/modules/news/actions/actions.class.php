<?php

require_once dirname(__FILE__) . '/../lib/newsGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/newsGeneratorHelper.class.php';

/**
 * news actions.
 *
 * @package    SNGT
 * @subpackage news
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends autoNewsActions
{
}

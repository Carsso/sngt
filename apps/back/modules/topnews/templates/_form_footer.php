<div class="clear"></div>
<div>
  <?php $array_images = misc::getImagesGames() ?>
  <?php foreach ($array_images as $image): ?>
    <div style="float:left;margin:20px;">
      <p>
        <?php echo image_tag('topnews/' . $image, array('width'=>'450px', 'height'=>'165px')) ?><br/>
        <?php echo $image ?>
      </p>
    </div>
  <?php endforeach; ?>
</div>
<div class="clear"></div>
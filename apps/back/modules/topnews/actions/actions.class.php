<?php

require_once dirname(__FILE__) . '/../lib/topnewsGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/topnewsGeneratorHelper.class.php';

/**
 * topnews actions.
 *
 * @package    SNGT
 * @subpackage topnews
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class topnewsActions extends autoTopnewsActions
{
}

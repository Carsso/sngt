<?php

require_once dirname(__FILE__) . '/../lib/cmsGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/cmsGeneratorHelper.class.php';

/**
 * cms actions.
 *
 * @package    SNGT
 * @subpackage cms
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cmsActions extends autoCmsActions
{
}

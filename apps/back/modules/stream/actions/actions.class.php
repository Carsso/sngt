<?php

require_once dirname(__FILE__) . '/../lib/streamGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/streamGeneratorHelper.class.php';

/**
 * stream actions.
 *
 * @package    SNGT
 * @subpackage stream
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class streamActions extends autoStreamActions
{
}

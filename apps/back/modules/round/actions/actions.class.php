<?php

require_once dirname(__FILE__) . '/../lib/roundGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/roundGeneratorHelper.class.php';

/**
 * round actions.
 *
 * @package    SNGT
 * @subpackage round
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class roundActions extends autoRoundActions
{
}

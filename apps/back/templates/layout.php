<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="<?php echo image_path('../favicon.ico') ?>"/>
  <?php include_stylesheets() ?>
  <?php include_javascripts() ?>
</head>
<body>
<div id="global">
  <?php
  if ($sf_user->hasFlash('info')) {
    echo '<div class="alert alert-info" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('info') . '</div>';
  }
  if ($sf_user->hasFlash('error')) {
    echo '<div class="alert alert-error" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('error') . '</div>';
  }
  if ($sf_user->hasFlash('success')) {
    echo '<div class="alert alert-success" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('success') . '</div>';
  }
  ?>
  <div id="header" class="clear" style="cursor:pointer;">
    <div class="content-inside">
    </div>
  </div>
  <div class="navbar clear all_col">
    <div class="navbar-inner" style="border-radius: 0">
      <div class="content-inside">
        <div class="container">
          <div class="nav-collapse">
            <ul class="nav">
              <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">News<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo url_for('@news') ?>">News</a></li>
                  <li><a href="<?php echo url_for('@top_news') ?>">Top News</a></li>
                </ul>
              </li>
              <li><a href="<?php echo url_for('@cms') ?>">CMS</a></li>
              <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Steps<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo url_for('@step') ?>">Steps</a></li>
                  <li><a href="<?php echo url_for('@game') ?>">Games</a></li>
                  <li><a href="<?php echo url_for('@round') ?>">Rounds</a></li>
                  <li><a href="<?php echo url_for('@team') ?>">Teams</a></li>
                  <li><a href="<?php echo url_for('@player') ?>">Players</a></li>
                </ul>
              </li>
              <li><a href="<?php echo url_for('@stream') ?>">Streams</a></li>
              <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo url_for('@sf_guard_user_profile') ?>">Profiles</a></li>
                  <li><a href="<?php echo url_for('@sf_guard_user') ?>">Users</a></li>
                  <li><a href="<?php echo url_for('@sf_guard_group') ?>">Groups</a></li>
                  <li><a href="<?php echo url_for('@sf_guard_permission') ?>">Permissions</a></li>
                </ul>
              </li>
              <li><a href="<?php echo url_for('@recap') ?>">Récapitulatifs</a></li>
            </ul>
            <ul class="nav pull-right">
              <?php
              if ($sf_user->isAuthenticated()) {
                ?>
                <li><a href="<?php echo url_for('@sf_guard_signout') ?>" title="<?php echo $sf_user->getUsername() ?>">Déconnexion</a>
                </li>
              <?php
              } else {
                ?>
                <li><a href="<?php echo url_for('@sf_guard_signin') ?>">Connexion</a></li>
              <?php
              }
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="content">
    <div class="content-inside">
      <?php echo $sf_content ?>
      <div class="clear"></div>
    </div>
  </div>
  <script type="text/javascript">
    $('#header').click(function () {
      $(window).attr('location', '<?php echo url_for('@homepage') ?>');
    });
  </script>
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-7726577-4']);
    _gaq.push(['_trackPageview']);

    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();

  </script>
</body>
</html
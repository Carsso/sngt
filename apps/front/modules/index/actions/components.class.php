<?php
class indexComponents extends sfComponents
{
  public function executeRegister()
  {
    $this->loggedIn = $this->getUser()->isAuthenticated();
    if (!$this->loggedIn) {
      $this->form = $this->newForm('applyForm');
    }
  }

  /**
   * Method that creates forms
   * @param sfForm $formClass
   * @param object $object
   * @return class
   */
  protected function newForm($formClass, $object = null)
  {
    $key = 'app_sfForkedApply_' . $formClass;
    $class = sfConfig::get($key);
    if ($object !== null) {
      return new $class($object);
    }
    return new $class();
  }
}

<?php

/**
 * index actions.
 *
 * @package    SNGT
 * @subpackage index
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class indexActions extends sfActions
{
  public function executeTest500(sfWebRequest $request)
  {
    throw new sfException("Test Exception");
  }

  public function executeLogin(sfWebRequest $request)
  {
    if($this->getUser()->isAuthenticated()){
      $this->redirect('@homepage');
    }
  }

  public function executeContact(sfWebRequest $request)
  {
    $this->hideform = false;
    if ($request->isMethod('post')) {
      $admins = Doctrine_Query::create()->select('email_address ')->from('sfGuardUser')->where('is_super_admin = 1')->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
      $destinataires = $admins;
      $subject = $request->getParameter('subject', null);
      $content = $request->getParameter('content', null);
      $expediteur = array($request->getParameter('from', null) => $request->getParameter('name', null));
      if ($subject && $content && $expediteur) {
        $this->hideform = true;
        $context = sfContext::getInstance();
        $message = $context->getMailer()->compose($expediteur,
          $destinataires,
            '[SNGT-Contact] ' . $subject,
          $content);
        $message = $message->setReplyTo($request->getParameter('from', null), $request->getParameter('name', null));
        $context->getMailer()->send($message);
      }
    }
  }

  public function executeError(sfWebRequest $request)
  {
  }

  public function executeSplash(sfWebRequest $request)
  {
    $this->setLayout('splash');
    $this->splash = Doctrine_Core::getTable('Cms')->findOneBy('slug', 'splash');
    if (!$this->splash) {
      $this->forward404('no splash');
    }
  }

  public function executeSettings(sfRequest $request)
  {
    // sfApplySettingsForm inherits from sfApplyApplyForm, which
    // inherits from sfGuardUserProfile. That minimizes the amount
    // of duplication of effort. If you want, you can use a different
    // form class. I suggest inheriting from sfApplySettingsForm and
    // making further changes after calling parent::configure() from
    // your own configure() method.

    $profile = $this->getUser()->getProfile();
    // we're getting default or customized settingsForm for the task
    if (!(($this->form = $this->newForm('settingsForm', $profile)) instanceof sfGuardUserProfileForm)) {
      // if the form isn't instance of sfApplySettingsForm, we don't accept it
      throw new InvalidArgumentException(sfContext::getInstance()->
            getI18N()->
            __('The custom %action% form should be instance of %form%',
              array('%action%' => 'settings',
                '%form%' => 'sfApplySettingsForm'), 'sfForkedApply')
      );
    }
    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
      if ($this->form->isValid()) {
        $this->form->save();
        return $this->redirect('@homepage');
      }
    }
  }
  protected function newForm($formClass, $object = null)
  {
    $key = 'app_sfForkedApply_' . $formClass;
    $class = sfConfig::get($key);
    if ($object !== null) {
      return new $class($object);
    }
    return new $class();
  }
}

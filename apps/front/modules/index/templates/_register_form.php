<form method="post" action="<?php echo url_for('sfApply/apply') ?>"
      name="sf_apply_apply_form" id="sf_apply_apply_form" class="form-horizontal">
  <fieldset class="fieldsetwithtitle">
    <div class="legend">Informations de connexion</div>
    <div class="control-group <?php echo ($form['username']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['username']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['username'] ?>
        <span class="help-inline"><?php echo $form['username']->renderError() ?></span>
        <span class="help-block">Votre pseudo de joueur (sans le nom de votre équipe).</span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['email']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['email']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['email'] ?>
        <span class="help-inline"><?php echo $form['email']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['email2']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['email2']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['email2'] ?>
        <span class="help-inline"><?php echo $form['email2']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['password']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['password']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password'] ?>
        <span class="help-inline"><?php echo $form['password']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['password2']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['password2']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password2'] ?>
        <span class="help-inline"><?php echo $form['password2']->renderError() ?></span>
      </div>
    </div>
  </fieldset>
  <fieldset class="fieldsetwithtitle">
    <div class="legend">Informations personnelles</div>
    <div class="control-group <?php echo ($form['lastname']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['lastname']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['lastname'] ?>
        <span class="help-inline"><?php echo $form['lastname']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['firstname']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['firstname']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['firstname'] ?>
        <span class="help-inline"><?php echo $form['firstname']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['birthday']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['birthday']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['birthday'] ?>
        <span class="help-inline"><?php echo $form['birthday']->renderError() ?></span>
        <span class="help-block">Veillez à bien remplir ce champ, des informations importantes vous seront indiquées en fonction de sa valeur.</span>
      </div>
    </div>
    <p class="alert alert-danger mineuronly hidden">
      ATTENTION: Vous êtes mineur, vous devez impérativement être accompagné d'une personne majeure pendant toute la durée de l'évènement.<br />
      Veuillez fournir une <a href="http://sngt.fr/uploads/decharge2013.pdf" target="_blank" class="btn btn-danger">décharge de responsabilité</a> remplie par votre responsable légal ainsi qu'une photocopie de sa pièce d'identité.</a>
    </p>
    <div class="control-group <?php echo ($form['phone']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['phone']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['phone'] ?>
        <span class="help-inline"><?php echo $form['phone']->renderError() ?></span>
        <span class="help-block">Il sera uniquement utilisé afin de vous contacter le jour de l'évènement si nécessaire.</span>
      </div>
    </div>
    <hr/>
    <div class="control-group <?php echo ($form['address']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['address']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['address'] ?>
        <span class="help-inline"><?php echo $form['address']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['postal_code']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['postal_code']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['postal_code'] ?>
        <span class="help-inline"><?php echo $form['postal_code']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['city']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['city']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['city'] ?>
        <span class="help-inline"><?php echo $form['city']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['country']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['country']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['country'] ?>
        <span class="help-inline"><?php echo $form['country']->renderError() ?></span>
      </div>
    </div>
  </fieldset>
  <fieldset class="fieldsetwithtitle">
    <div class="legend">Informations d'identité</div>
    <p>
      Les informations de votre pièce d'identité vous sont demandées afin d'accelèrer le traitement à votre arrivée et ainsi diminuer le temps d'attente (les personnes ayant rempli cette partie seront traités en priorité).<br />
      ATTENTION: Remplir ce formulaire ne vous dispense pas d'ammener votre pièce d'identité avec vous le jour de l'évènement.
    </p>
    <div class="control-group <?php echo ($form['identity_card_type']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['identity_card_type']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['identity_card_type'] ?>
        <span class="help-inline"><?php echo $form['identity_card_type']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group <?php echo ($form['identity_card_type_other']->renderError()) ? 'error' : '' ?> hidden">
      <?php echo $form['identity_card_type_other']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['identity_card_type_other'] ?>
        <span class="help-inline"><?php echo $form['identity_card_type_other']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group donot <?php echo ($form['identity_card_delivery']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['identity_card_delivery']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['identity_card_delivery'] ?>
        <span class="help-inline"><?php echo $form['identity_card_delivery']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group donot <?php echo ($form['identity_card_authority']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['identity_card_authority']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['identity_card_authority'] ?>
        <span class="help-inline"><?php echo $form['identity_card_authority']->renderError() ?></span>
        <span class="help-block">Exemple: Préfecture de la Côte d'Or (21), Sous-préfecture de Chalon-sur-Saône (71)</span>
      </div>
    </div>
    <div class="control-group donot <?php echo ($form['identity_card']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['identity_card']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['identity_card'] ?>
        <span class="help-inline"><?php echo $form['identity_card']->renderError() ?></span>
        <span class="help-block">
          <div class="cnionly hidden">
            Pour les Cartes d'identité Françaises:<br />
            Merci de bien vouloir indiquer la clé de sécurité de votre carte à la suite de votre numéro, sans espace ni tiret.<br />
            Si vous ne savez pas comment la trouver, <a href="#modalcni" role="button" class="btn btn-mini btn-danger" data-toggle="modal">cliquez ici</a>.<br />
            Exemple : Si 010101123123 est mon numéro de carte et 8 est ma clé de sécurité, j'entre 0101011231238
          </div>
        </span>
      </div>
    </div>
    <p class="doyes">
      ATTENTION: Les personnes ayant indiqué leur pièce d'identité seront traitées en priorité à leur arrivée.<br />
    </p>
  </fieldset>
  <div id="modalcni" class="modal hide fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">×</button>
      <h3 id="myModalLabel">Comment trouver le numéro et la clé de sécurité de votre Carte d'identité Française</h3>
    </div>
    <div class="modal-body">
      <p>
        <?php echo image_tag('carte_identite.png') ?>
      </p>
      <p>
        <strong>Le numéro de la carte d'identité est souligné en rouge sur l'image ci-dessus.</strong><br />
        Il est indiqué à la fois en haut et en bas a droite de votre carte.<br />
      </p>
      <p>
        <strong>La clé de sécurité est entourée en vert sur l'image ci-dessus.</strong><br />
        Elle est composée d'un chiffre et est située entre votre numéro de carte d'identité et votre prénom en bas à droite de la carte.
      </p>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal">Close</button>
    </div>
  </div>
  <fieldset class="fieldsetwithtitle">
    <div class="legend">Informations complémentaires</div>
    <div class="control-group <?php echo ($form['masters_license']->renderError()) ? 'error' : '' ?>">
      <?php echo $form['masters_license']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['masters_license'] ?>
        <span class="help-inline"><?php echo $form['masters_license']->renderError() ?></span>
      </div>
    </div>
  </fieldset>
  <?php echo $form->renderHiddenFields(false) ?>
  <div class="form-actions actions sf_admin_actions">
    <input type="submit" id="register_submit_button" title="Ce bouton s'active automatiquement une fois que tous les champs sont correctement remplis." value="<?php echo __("Create My Account", array(), 'sfForkedApply') ?>"
           class="btn btn-primary" >
    <?php echo link_to(__("Cancel", array(), 'sfForkedApply'), sfConfig::get('app_sfForkedApply_after', sfConfig::get('app_sfApplyPlugin_after', '@homepage')), array('class' => 'btn')) ?>
  </div>

</form>
<script type="text/javascript">
  $(document).ready(function () {
    $('#sfApplyApply_firstname').autotab_filter({ format: 'all', camelcase: true });
    $('#sfApplyApply_lastname').autotab_filter({ format: 'all', uppercase: true });
    $('#sfApplyApply_city').autotab_filter({ format: 'all', uppercase: true });
    $('#sfApplyApply_country').autotab_filter({ format: 'all', uppercase: true });
    $('#sfApplyApply_postal_code').autotab_filter({ format: 'alphanumeric', uppercase: true });
    $('#sfApplyApply_identity_card').autotab_filter({ format: 'alphanumeric', uppercase: true });
    $('#sfApplyApply_address').autotab_filter({ format: 'all', camelcase: true });
    $('#sfApplyApply_identity_card_authority').autotab_filter({ format: 'all', camel: true });
    $('#sfApplyApply_email').autotab_filter({ format: 'custom', lowercase: true });
    $('#sfApplyApply_email2').autotab_filter({ format: 'custom', lowercase: true });
    $('#sfApplyApply_phone').autotab_filter({ format: 'custom', pattern: '[^0-9+]' });
    identity_update();
  });
  $('#sfApplyApply_identity_card_type').change(function () {
    identity_update();
  });
  $('#sfApplyApply_identity_card').keyup(function () {
    identity_update();
  }).change(function () {
    identity_update();
  });
  $('#sfApplyApply_identity_card_delivery_month').change(function () {
    identity_update();
  });
  $('#sfApplyApply_identity_card_delivery_year').change(function () {
    identity_update();
  });
  function identity_update() {
    if ($('#sfApplyApply_identity_card_type').val() == 'other') {
      $('#sfApplyApply_identity_card_type_other').parent('.controls').parent('.control-group').show();
      $('#sfApplyApply_identity_card').parent('div.controls').parent('div.control-group').removeClass('error');
      $('.cnionly').hide();
      doYes();
      $('#sfApplyApply_identity_card_type_other').attr('required','required');
      $('#register_submit_button').attr('disabled',false);
    } else {
      $('#sfApplyApply_identity_card_type_other').parent('.controls').parent('.control-group').hide();
      if ($('#sfApplyApply_identity_card_type').val() == 'cni') {
        $('.cnionly').show();
        doYes();
        var identity_num = $('#sfApplyApply_identity_card').val();
        var identity_month = $('#sfApplyApply_identity_card_delivery_month').val();
        var identity_year = $('#sfApplyApply_identity_card_delivery_year').val();
        var identity_month_number = parseInt(identity_month,10)
        var identity_year_number = parseInt(identity_year.substr(2,2),10);
        var identity_num_month = parseInt(identity_num.substr(2,2),10);
        var identity_num_year = parseInt(identity_num.substr(0,2),10);
        var identity_num_key = identity_num.substr(-1,1);
        var identity_num_numonly = identity_num.substr(0,identity_num.length-1);
        logMe('Month (from field) : '+identity_month_number);
        logMe('Month (from card number) : '+identity_num_month);
        logMe('Year (from field) : '+identity_year_number);
        logMe('Year (from card number) : '+identity_num_year);
        logMe('Card number only : '+identity_num_numonly);
        logMe('Control key only (from card number field) :  '+identity_num_key);
        logMe('Control key only (from control function) :  '+cleDeControle(identity_num_numonly));
        logMe('String length : '+identity_num.length)
        if(identity_num.length >= 13 && identity_num_year == identity_year_number && identity_num_month == identity_month_number && cleDeControle(identity_num_numonly) == identity_num_key){
          $('#sfApplyApply_identity_card').parent('div.controls').parent('div.control-group').removeClass('error');
          $('#register_submit_button').attr('disabled',false);
        } else {
          $('#sfApplyApply_identity_card').parent('div.controls').parent('div.control-group').addClass('error');
          $('#register_submit_button').attr('disabled','disabled');
        }
      } else {
        if ($('#sfApplyApply_identity_card_type').val() == 'no') {
          $('#sfApplyApply_identity_card').parent('div.controls').parent('div.control-group').removeClass('error');
          $('.cnionly').hide();
          doNot();
          $('#register_submit_button').attr('disabled',false);
        } else {
          $('#sfApplyApply_identity_card').parent('div.controls').parent('div.control-group').removeClass('error');
          $('.cnionly').hide();
          doYes();
          $('#register_submit_button').attr('disabled',false);
        }
      }
    }
  }

  function doNot(){
    $('.donot').hide();
    $('.doyes').show();
    $('#sfApplyApply_identity_card').attr('required',false);
    $('#sfApplyApply_identity_card_authority').attr('required',false);
    $('#sfApplyApply_identity_card_delivery_day').attr('required',false);
    $('#sfApplyApply_identity_card_delivery_month').attr('required',false);
    $('#sfApplyApply_identity_card_delivery_year').attr('required',false);
    $('#sfApplyApply_identity_card_type_other').attr('required',false);
  }

  function doYes(){
    $('.donot').show();
    $('.doyes').hide();
    $('#sfApplyApply_identity_card').attr('required','required');
    $('#sfApplyApply_identity_card_authority').attr('required','required');
    $('#sfApplyApply_identity_card_delivery_day').attr('required','required');
    $('#sfApplyApply_identity_card_delivery_month').attr('required','required');
    $('#sfApplyApply_identity_card_delivery_year').attr('required','required');
    $('#sfApplyApply_identity_card_type_other').attr('required',false);
  }

  function cleDeControle(chars){
    var factor = [7, 3, 1];
    var result = 0;
    chars = chars.toUpperCase();
    for(var i= 0; i < chars.length; i++)
    {
      var index = i;
      var char = chars[i];
      if(char == '<'){
        char = 0;
      } else {
        if(char.charCodeAt(0) >= 65 && char.charCodeAt(0) <= 90){
          char = char.charCodeAt(0) - 55;
        } else {
          char = parseInt(char);
        }
      }
      result += char * factor[index % 3]
    }
    return result % 10
  }



  $('#sfApplyApply_birthday_day').change(function () {
    naissance_update();
  });
  $('#sfApplyApply_birthday_month').change(function () {
    naissance_update();
  });
  $('#sfApplyApply_birthday_year').change(function () {
    naissance_update();
  });
  function naissance_update() {
    var birth_day = $('#sfApplyApply_birthday_day').val();
    var birth_month = $('#sfApplyApply_birthday_month').val();
    var birth_year = $('#sfApplyApply_birthday_year').val();
    if(birth_day && birth_month && birth_year){
      var birthdate=new Date()
      birthdate.setFullYear(birth_year);
      birthdate.setMonth(birth_month-1);
      birthdate.setDate(birth_day);
      var dtime = strtotime('+18 years',birthdate.getTime()/1000)*1000;
      var now = new Date()
      var timenow = now.getTime();
      logMe('Birth day:');
      logMe(birth_day);
      logMe('Birth month:');
      logMe(birth_month);
      logMe('Birth year:');
      logMe(birth_year);
      logMe('Birthdate:');
      logMe(new Date(birthdate));
      logMe('Birthdate +18y:');
      logMe(new Date(dtime));
      logMe('Now:');
      logMe(now);
      if(dtime>now){
        $('.mineuronly').show();
      } else {
        $('.mineuronly').hide();
      }
    } else {
      $('.mineuronly').hide();
    }
  }
</script>
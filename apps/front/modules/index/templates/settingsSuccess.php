<?php slot('sf_apply_login') ?>
<?php use_stylesheets_for_form($form) ?>
<?php end_slot() ?>
<?php use_helper("I18N") ?>
<div class="left_col column">
  <div class="sf_apply sf_apply_settings boxed">
    <h2><?php echo __("Account Settings", array(), 'sfForkedApply') ?></h2>
    <?php if (0): ?>
      <form method="post" action="<?php echo url_for("sfApply/settings") ?>" name="sf_apply_settings_form"
            id="sf_apply_settings_form" class="form-horizontal">
        <?php echo $form ?>
        <div class="form-actions actions sf_admin_actions">
          <input type="submit" value="<?php echo __("Save", array(), 'sfForkedApply') ?>" class="btn btn-primary">
          <?php echo link_to(__('Cancel', array(), 'sfForkedApply'), sfConfig::get('app_sfForkedApply_after', sfConfig::get('app_sfApplyPlugin_after', '@homepage')), array('class' => 'btn')) ?>
        </div>
      </form>
    <?php endif; ?>
    <form method="GET" action="<?php echo url_for("sfApply/resetRequest") ?>" name="sf_apply_reset_request"
          id="sf_apply_reset_request" class="form-horizontal">
      <p>
        <?php echo __('Click the button below to change your password.', array(), 'sfForkedApply'); ?>
        <?php
        $confirmation = sfConfig::get('app_sfForkedApply_confirmation');
        if ($confirmation['reset_logged']): ?>
          <?php echo __('For security reasons, you
will receive a confirmation email containing a link allowing you to complete the password change.', array(), 'sfForkedApply') ?>
        <?php endif; ?>
      </p>
      <input type="submit" value="<?php echo __("Reset Password", array(), 'sfForkedApply') ?>" class="btn btn-danger"/>
    </form>
  </div>
</div>
<div class="right_col column" style="min-height: 450px">
  <?php if ($sf_user->isAuthenticated()): ?>
    <p class="big" style="margin-top: 40px;text-align: center;">
      Votre Pseudo :<br />
      <?php echo $sf_user->getGuardUser() ?>
    </p>
  <?php endif; ?>
</div>

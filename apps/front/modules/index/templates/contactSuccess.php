<div class="left_col column">
  <?php if (!$hideform): ?>
    <h1>Contact</h1>

    <form action="" method="post" class="form-horizontal" style="margin-top: 20px;">

      <div class="control-group">
        <label class="control-label">Votre nom:</label>

        <div class="controls">
          <input type="text" name="name" style="width:300px;"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Votre adresse email:</label>

        <div class="controls">
          <input type="text" name="from" style="width:300px;"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Sujet:</label>

        <div class="controls">
          <input type="text" name="subject" style="width:300px;"/>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Message:</label>

        <div class="controls">
          <textarea name="content" style="width:300px;" rows="10"></textarea>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" value="Envoyer le message"/>
        </div>
      </div>
    </form>
  <?php else: ?>
    <p class="big" style="margin-top: 60px;text-align: center;">
      Votre message à bien été envoyé, merci.
    </p>
  <?php endif; ?>
</div>
<div class="right_col column">
  <div class="boxed" style="padding: 10px;">
    <h3 style="margin-top: 40px;">Pensez à consulter la FAQ</h3>

    <p>Avant de nous contacter, pensez à consulter la FAQ, vous y trouverez les réponses aux questions les plus
      fréquemment posées.</p>

    <h3 style="margin-top: 40px;">Contactez-nous par téléphone</h3>

    <p style="margin-bottom: 0;">
      <span style="float:left;margin-right:5px;">
        <?php echo image_tag('cuteoperator.jpg', array('width' => '94px', 'height' => '81px')) ?>
      </span>
      Une question, un problème ? Vous pouvez également nous contacter par téléphone !
    </p>

    <p class="big" style="line-height:16px;">
      <a href="callto:0972290797" id="call_number_link"
         style="margin:5px 16px 0;display:block;">
        09 72 29 07 97
      </a>
    </p>
  </div>
</div>
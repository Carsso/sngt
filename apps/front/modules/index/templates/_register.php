<?php if ($loggedIn): ?>
  <?php include_partial('sfApply/logoutPrompt') ?>
<?php else: ?>
  <?php include_partial('index/register_form', array('form' => $form)) ?>
<?php endif; ?>
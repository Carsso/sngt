<div class="boxed">
  <h2>Mes inscriptions</h2>
  <?php $noround = true; ?>
  <?php foreach ($sf_user->getGuardUser()->getPlayer() as $player): ?>
    <?php $team = $player->getTeam(); ?>
    <?php $round = $team->getRound(); ?>
    <?php $game = $round->getGame(); ?>
    <?php $step = $round->getStep(); ?>
    <?php if ($player->getIsPayed()): ?>
      <?php $noround = false; ?>
      <div class="p_center">
        <p>
          <a href="#modalstepplayer<?php echo $player->getId() ?>" role="button" class="btn btn-mini btn-sngt"
             data-toggle="modal">
            <?php echo $step ?>
          </a>
        </p>
      </div>
      <div id="modalstepplayer<?php echo $player->getId() ?>" class="modal hide fade" tabindex="-1" role="dialog"
           style="display: none;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h3 id="myModalLabel"><?php echo $step ?></h3>
        </div>
        <div class="modal-body">
          <p>
            Vous êtes inscrit pour <strong><?php echo $game ?></strong>.<br/>
            Vous êtes membre de l'équipe <strong><?php echo $team ?></strong>.
          </p>

          <p>
            Voici la constitution actuelle de l'équipe :<br/>
            <?php foreach ($team->getPlayer() as $team_player): ?>
              <?php if ($team_player->getIsPayed()): ?>
                - <?php echo $team_player->getSfGuardUser() ?> :
                <span style="color:green">Payé</span>
                <br/>
              <?php endif; ?>
            <?php endforeach; ?>
          </p>

          <p>
            <a href="<?php echo url_for('etape', $step) ?>" class="btn btn-primary">Accèder à la page de l'étape</a>
          </p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal">Close</button>
        </div>
      </div>
      <hr class="pinksep" />
    <?php endif; ?>
  <?php endforeach; ?>
  <?php if ($noround): ?>
    <div class="p_center">
      <p class="big">
        Aucune inscription
      </p>
    </div>
    <hr/>
  <?php endif; ?>
  <?php if ($the_next_step && (!$sf_user->getGuardUser()->isInscrit($the_next_step->getId())) && ($the_next_step->getStatus() == 1)): ?>
    <div class="p_center">
      <p style="margin-top:15px;">
        <a href="<?php echo url_for('etape', $the_next_step) ?>" class="btn btn-sngt" style="color:white;">
          S'inscrire à l'étape de <?php echo $the_next_step->getCity() ?>
        </a>
      </p>
    </div>
  <?php endif; ?>
</div>
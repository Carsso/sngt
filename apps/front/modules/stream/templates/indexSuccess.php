<div class="newsh"></div>
<div class="newsc">
  <div class="titlenews"><?php echo ($cms) ? $cms->getTitle() : 'Streaming' ?></div>
  <div class="texte">
    <?php echo ($cms) ? sfOutputEscaper::unescape($cms->getContent()) : '' ?>
    <div class="stream_btns">
      <?php
      $first = false;
      foreach ($streams as $stream) {
        if (!$first) {
          $first = $stream;
        }
        ?>
        <span class="stream_div">
            <a href="<?php echo $stream->getUrl() ?>" target="stream_iframe"
               class="btn btn-danger btn-large"><?php echo $stream->getName() ?></a>
          </span>
      <?php
      }
      ?>
      <div class="clear"></div>
    </div>
    <?php if ($first): ?>
      <div class="rounded" style="background-color:#000000;text-align:center;">
        <iframe name="stream_iframe" src="<?php echo $first->getUrl() ?>" width="730" height="360" frameborder="0"
                scrolling="no" style="margin:10px"></iframe>
      </div>
    <?php else: ?>
      <div style="margin-left:10px;">
        <p>Aucun stream</p>
      </div>
    <?php endif; ?>
  </div>
</div>
<div class="newsb"></div>

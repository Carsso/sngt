<?php

/**
 * stream actions.
 *
 * @package    SNGT
 * @subpackage stream
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class streamActions extends sfActions
{
  /**
   * Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    $this->streams = Doctrine_Query::create()
        ->from('Stream')
        ->orderBy('elem_order, name')
        ->execute();
    $this->cms = Doctrine_Core::getTable('Cms')->findOneBy('slug', 'stream');
  }
}

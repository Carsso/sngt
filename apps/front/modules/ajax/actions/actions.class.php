<?php

/**
 * index actions.
 *
 * @package    SNGT
 * @subpackage ajax
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ajaxActions extends sfActions
{
  public function executeEquipe(sfWebRequest $request)
  {
    $round_id = $request->getParameter('round');
    $team_name = strtolower(Doctrine_Inflector::urlize($request->getParameter('name')));
    $round = Doctrine::getTable('Round')->find($round_id);
    $team = Doctrine_Query::create()
        ->from('Team')
        ->where('round_id = ?', $round_id)
        ->addWhere('name LIKE ?', $team_name)
        ->fetchOne();
    if (is_object($team)) {
      if (!$team->getIsReserved()) {
        foreach ($team->getPlayer() as $player) {
          $player->delete();
        }
        $team->delete();
      }
    }
    $team = Doctrine_Query::create()
        ->from('Team')
        ->where('round_id = ?', $round_id)
        ->addWhere('name LIKE ?', $team_name)
        ->fetchOne();
    if (is_object($team)) {
      echo $team->getName();
    } elseif ($round->getNbTeamFree() <= 0) {
      echo '-1';
    } else {
      echo '0';
    }
    die();
  }
}

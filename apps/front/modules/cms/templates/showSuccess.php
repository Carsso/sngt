<div class="boxed">
  <h2><?php echo $cms->getTitle() ?></h2>

  <div style="margin-left:10px;">
    <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($cms->getContent()), false, false) ?>
  </div>
</div>

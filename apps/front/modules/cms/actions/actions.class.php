<?php

/**
 * cms actions.
 *
 * @package    SNGT
 * @subpackage cms
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cmsActions extends sfActions
{
  public function executeShow(sfWebRequest $request)
  {
    $this->cms = $this->getRoute()->getObject();
  }
  public function executeReglement(sfWebRequest $request)
  {
    $this->reglement = Doctrine_Core::getTable('Cms')->findOneBy('slug', 'reglement');
  }
}

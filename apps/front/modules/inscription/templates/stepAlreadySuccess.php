<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<?php if ($sf_user->isAuthenticated()): ?>
  <div class="boxed">
      <p class="big">Désolé, vous êtes déjà inscrit pour cette étape pour le jeu <?php echo $player->getTeam()->getRound()->getGame() ?> avec l'équipe <?php echo $player->getTeam() ?>!</p>
  </div>
<?php endif; ?>
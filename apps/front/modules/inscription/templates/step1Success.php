<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<?php if ($sf_user->isAuthenticated()): ?>
  <div class="lightblue borderwhite" style="margin-bottom:10px;padding:5px;">
    <div class="reveal_choice">
      <p class="big">J'ai déjà un compte sur le site (Connexion)</p>
    </div>
    <div class="reveal_choice_one p_center">
      <p class="big" style="margin-top: 30px;">
        Vous êtes actuellement connecté en tant que <?php echo $sf_user ?>.
      </p>

      <form class="form-horizontal" method="post" style="margin-bottom: 0;">
      <div class="form-actions actions sf_admin_actions form-actions-right" style="margin-bottom: 0;">
        <input type="submit" class="btn btn-primary btn-submit" value="Etape Suivante">
      </div>
        </form>
    </div>
  </div>
<?php else: ?>
  <div class="lightblue borderwhite reveal_choice_parent reveal_choice_parent_hidden" style="margin-bottom:10px;padding:5px;">
    <div class="reveal_choice">
      <p class="big" style="margin-bottom:0;">J'ai déjà un compte sur le site (Connexion)</p>
    </div>
    <div class="reveal_choice_one reveal_register toreveal hidden">
      <?php include_component('sfApply', 'login') ?>
      <style type="text/css">
        .reveal_register .sf_admin_actions .btn-success {
          display:none;
        }
      </style>
    </div>
  </div>
  <div class="lightblue borderwhite reveal_choice_parent reveal_choice_parent_hidden" style="margin-bottom:10px;padding:5px;">
    <div class="reveal_choice">
      <p class="big" style="margin-bottom:0;">Je n'ai pas encore de compte (Inscription)</p>
    </div>
    <div class="reveal_choice_one reveal_login toreveal hidden">
      <?php include_component('index', 'register') ?>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function () {
      $('.reveal_choice_parent_hidden').click(function () {
        if ($(this).hasClass('reveal_choice_parent_hidden')) {
          $('.toreveal').hide();
          $('.reveal_choice_parent').removeClass('reveal_choice_parent_hidden').addClass('reveal_choice_parent_hidden');
          $(this).removeClass('reveal_choice_parent_hidden').children('.toreveal').show();
        }
      });
    });
  </script>
<?php endif; ?>
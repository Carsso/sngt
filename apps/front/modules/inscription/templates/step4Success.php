<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<div class="boxed" style="text-align: center;">
  <h2>Paiement de votre place</h2>
  <p>
    Merci de bien lire les indications indiquées ci-dessous :
  </p>
  <p>
    1 : Cliquez sur "Ouvrir la fenêtre de paiement"
  </p>
  <p>
    2 : Laissez vous guider sur le site de Yurplan<br />
      <small>Nous avons pré-rempli les infomations, vous ne devez EN AUCUN CAS les modifier.<br />
      Dans le cas contraire votre inscription ne pourra pas être validée.</small><br />
  </p>
  <p>
    3 : Une fois le paiement effectué, fermez la popup, la vérification du paiement est automatique<br >
    <small>Vous pouvez également cliquer sur le bouton "J'ai payé" si rien ne se passe</small>
  </p>
  <form class="form-horizontal">
    <div class="control-group">
      <div class="controls" style="margin-left: 100px;">
        <label class="checkbox">
          <input type="checkbox" id="chkbx" value="1"> J'ai compris la procédure indiquée et dégage les organisateurs de toute responsabilité en cas de problème si je ne m'y confirme pas
        </label>
      </div>
    </div>
  </form>
  <p id="payment_btn" class="hidden">
    <a id="btn-openyur" href="<?php echo $yurplanurl ?>/event/SNGT/<?php echo $round->getYurplanEventId() ?>/tickets?lastname=<?php echo urlencode($profile->getLastname()) ?>&firstname=<?php echo urlencode($profile->getFirstname()) ?>&email=<?php echo urlencode($guard->getEmailAddress()) ?>" target="_blank" class="btn btn-large btn-success">
      Ouvrir la fenêtre de paiement
    </a>
    <button class="btn btn-large btn-danger" id="btn-payed">
      J'ai payé
    </button>
  </p>
  <p id="payment_btn_fake">
    <button class="btn btn-large btn-success disabled">
      Ouvrir la fenêtre de paiement
    </button>
    <button class="btn btn-large btn-danger disabled">
      J'ai payé
    </button>
  </p>
  <p id="payment_txt">
  </p>

  <script type="text/javascript">
    var isp = false;
    var lock = false;
    $(document).ready(function(){
      $('#chkbx').click(function(){
        $('#payment_btn_fake').hide();
        $('#payment_btn').show();
        $(this).attr('disabled', true);
        setInterval(function () {
          checkYurplan();
        }, 11000);
        checkYurplan();
        $('#chkbx').unbind('click');
      });
      $('#btn-openyur').click(function(){
        window.open ($(this).attr('href'), 'YurPlan', config='height=800, width=1100, toolbar=no, menubar=no, scrollbars=auto, resizable=yes, location=no, directories=no, status=no')
        isp = true;
        return false;
      });
      $('#btn-payed').click(function(){
        if(isp){
          checkYurplan();
        } else {
          alert('Vous devez effectuer votre paiement sur YurPlan avant de cliquer sur "J\'ai payé"');
        }
      });
      function checkYurplan(){
        if(lock){
          $('#payment_txt').html('Vous devez patienter 10 secondes avant de pouvoir de nouveau vérifier votre paiement.');
          $('#payment_txt').css('color','blue');
        } else {
          $('#payment_txt').html('Vérification de votre paiement en cours...');
          $('#payment_txt').css('color','orange');
          lock = true;
          $.ajax({
            url: "<?php echo url_for('inscription_check_ajx', array('player_id' => $player->getId())) ?>",
            cache: false,
            success: function (html) {
              if(html=="1"){
                $('#payment_txt').html('Votre paiement est validé, redirection en cours... Si rien ne se passe, <a href="<?php echo url_for('inscription_step4', array('step_slug' => $step->getSlug())) ?>">cliquez ici</a>');
                $('#payment_txt').css('color','green');
                $(location).attr( 'href' , '<?php echo url_for('inscription_step4', array('step_slug' => $step->getSlug())) ?>' );
              } else {
                $('#payment_txt').html('Votre paiement n\'a pas été validé. Avez vous bien effectué vous paiement sur YurPlan ?<br />Patientez quelques instants ou contactez-nous pour plus d\'informations.');
                $('#payment_txt').css('color','red');
              }
            }
          });
          waiting_time = setTimeout(function () {
            lock = false;
          }, 10000);
        }
      }
    });
  </script>
</div>
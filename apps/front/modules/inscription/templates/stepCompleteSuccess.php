<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<div class="boxed">
  <h2>Jeu complet pour l'étape de <?php echo $step ?></h2>

  <p>
    Désolé, le jeu que vous avez choisi est complet pour cette étape.
  </p>

  <div class="form-actions actions sf_admin_actions form-actions-right" style="margin-bottom: 0;">
    <a href="<?php echo url_for('etape', $step) ?>" class="btn btn-primary btn-submit btn-large">Retourner à la page de l'étape de <?php echo $step ?></a>
  </div>

</div>
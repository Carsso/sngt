<div class="span12" style="margin-left: 15px;">
  <ul class="thumbnails" style="margin-bottom: 0;">
    <?php
    $names = array(
      'Choix de l\'étape',
      'Connexion/Inscription',
      'Sélection du jeu',
      'Récapitulatif',
      'Paiement',
      'Confirmation'
    )
    ?>
    <?php for ($i = 0; $i <= 5; $i++): ?>
      <li class="span2" style="margin-bottom: 0;">
        <div
            class="thumbnail <?php echo ($inscrstep == $i) ? 'inscr_current' : (($inscrstep > $i) ? 'inscr_past' : 'inscr_future') ?>">
          <div class="caption" style="text-align: center;">
            <p style="margin-bottom: 0;">
              Étape <?php echo $i ?><br/>
              <small><?php echo $names[$i] ?></small>
            </p>
          </div>
        </div>
      </li>
    <?php endfor; ?>
  </ul>
</div>
<div class="clearfix"></div>
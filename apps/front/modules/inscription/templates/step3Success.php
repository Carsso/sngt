<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<?php if ($sf_user->isAuthenticated()): ?>
  <form class="form-horizontal" method="post" style="margin: 0;">
  <div class="boxed">
    <h2>Règlement du SNGT - À LIRE IMPÉRATIVEMENT</h2>

      <div style="width:900px;height:400px; padding: 10px; border: 1px solid #ccc;margin:auto;overflow-x:scroll;">
        <?php if ($reglement): ?>
          <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($reglement->getContent())) ?>
        <?php else: ?>
          Règlement indisponible actuellement.
        <?php endif; ?>
      </div>
      <div class="control-group">
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" name="reglement" value="1"> J'ai lu et j'accepte le règlement du SNGT
          </label>
        </div>
      </div>
    <h2>Information Importante</h2>
    <p class="big">
      Pensez à vous munir de votre pièce d'identité, elle est obligatoire pour votre participation.
    </p>

    <p>
      ATTENTION: Les mineurs doivent impérativement être accompagné d'une personne majeure pendant toute la durée de l'évènement.<br />
      Si vous êtes mineur, vous devez fournir une <a href="http://sngt.fr/uploads/decharge2013.pdf" target="_blank">décharge de responsabilité</a> remplie par votre responsable légal ainsi qu'une photocopie de sa pièce d'identité.</a>
    </p>
  </div>
  <div class="boxed">
    <h2>Récapitulatif de réservation</h2>

    <p class="big">
      Équipe : <?php echo $team ?><br />
      Étape : <?php echo $team->getRound()->getStep() ?><br />
      Jeu : <?php echo $team->getRound()->getGame() ?><br />
    </p>
    <div class="form-actions actions sf_admin_actions form-actions-right" style="margin-bottom: 0;">
      <input type="submit" class="btn btn-primary btn-submit btn-large" value="Procèder au paiement">
    </div>
  </div>
  </form>

  <script type="text/javascript">
    $(document).ready(function () {
      $('form').each(function () {
        $(this).validate({
          errorElement: "span",
          errorClass: "help-inline error",
          validClass: "help-inline valid"
        });
      });
    });
  </script>
<?php endif; ?>
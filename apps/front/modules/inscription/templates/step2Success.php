<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<?php if ($sf_user->isAuthenticated()): ?>
  <h2>Choisissez votre jeu</h2>
  <?php foreach ($step->getRound() as $round): ?>
    <div class="lightblue borderwhite reveal_choice_parent reveal_choice_parent_hidden"
         style="margin-bottom:10px;padding:5px;">
      <div class="reveal_choice">
        <div class="float_right">
          <?php echo image_tag($round->getGame()->getImage(), array('width' => '48px', 'height' => '48px')) ?>
        </div>
        <div class="float_left" style="padding-top: 10px;padding-left: 10px;">
          <p class="big">
            <?php echo $round->getGame() ?>
            <small>
              <?php if ($round->getIsComplete()): ?>
                <span class="error">Complet</span>
              <?php else: ?>
                <?php echo $round->getNbTeamFree() ?> places restantes
              <?php endif; ?>
            </small>
          </p>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="reveal_choice_one toreveal hidden"
           style="margin-top: 20px; <?php if (is_object($teamcomplete) && ($teamcomplete->getRound()->getId() == $round->getId())): ?>display:block;<?php endif; ?>">
        <form class="form-horizontal" method="post" id="form_team" style="margin-bottom: 0;">
          <div class="control-group">
            <label class="control-label" for="inputTeam">Équipe</label>

            <div class="controls">
              <?php if ($round->getGame()->getTeamNbPlayers() == 1): ?>
                <span class="input-xlarge uneditable-input"><?php echo $sf_user->getUsername() ?></span>
                <input type="hidden" id="inputTeam" name="team" required="required"
                       value="<?php echo $sf_user->getUsername() ?>">
              <?php else: ?>
                <input type="text" id="inputTeam<?php echo $round->getId() ?>" title="Entrez votre nom d'équipe"
                       class="input-xlarge" name="team" required="required"
                       value="<?php if (is_object($teamcomplete)) {
                         echo $teamcomplete->getName();
                       } ?>"
                       placeholder="Nom de l'équipe" list="teams<?php echo $round->getId() ?>">
                <datalist id="teams<?php echo $round->getId() ?>">
                  <?php foreach($round->getTeam() as $team): ?>
                  <option value="<?php echo $team->getName() ?>">
                    <?php endforeach ?>
                </datalist>
                <?php if ($round->getNbTeamFree() <= 0): ?>
                  <span
                      class="help-block error">
                    ATTENTION: Il n'y a plus de nouvelles places d'équipe disponibles. Vous pouvez uniquement rejoindre votre équipe si elle est déjà inscrite.
                  </span>
                <?php endif; ?>
                <span
                    class="help-block" id="inputText<?php echo $round->getId() ?>">Le nom d'équipe peut contenir uniquement des lettres (sans accents), chiffres et tirets (-)</span>
                <div id="divTeam<?php echo $round->getId() ?>" style="margin-top: 20px;"></div>

                <script type="text/javascript">
                  var inputTeam<?php echo $round->getId() ?> = $('#inputTeam<?php echo $round->getId() ?>');
                  var divTeam<?php echo $round->getId() ?> = $('#divTeam<?php echo $round->getId() ?>');
                  var timeout<?php echo $round->getId() ?>;
                  $(document).ready(function () {
                    inputTeam<?php echo $round->getId() ?>.keyup(function () {
                      clearInterval(timeout<?php echo $round->getId() ?>);
                      timeout<?php echo $round->getId() ?> = setInterval(function () {
                        retrieveInfos<?php echo $round->getId() ?>(inputTeam<?php echo $round->getId() ?>.val());
                      }, 1000);
                    });
                    function retrieveInfos<?php echo $round->getId() ?>(name) {
                      $.ajax({
                        url: "<?php echo url_for('@inscription_team_players_ajx') ?>",
                        type: "POST",
                        data: { round_id: <?php echo $round->getId() ?>, team_name: name },
                        cache: false,
                        dataType: "json",
                        success: function (data) {
                          var result = "<strong>Liste des joueurs actuels de l'équipe " + name + " :</strong><br />";
                          $.each(data, function (key, val) {
                            result += " - Joueur n°" + (key + 1) + " : " + val + '<br />';
                          });
                          if (!data.length) {
                            result += "<em>Aucun joueur dans cette équipe !</em><br />";
                          }
                          divTeam<?php echo $round->getId() ?>.html(result);
                        },
                        error: function () {
                          divTeam<?php echo $round->getId() ?>.html('Une erreur est survenue !');
                        }
                      });

                    }
                  });
                </script>
              <?php endif; ?>

              <script type="text/javascript">
                $(document).ready(function () {
                  $('#inputTeam<?php echo $round->getId() ?>').autotab_filter({ format: 'custom', pattern: '[^a-zA-Z0-9-]' });
                });
              </script>
              <?php if (is_object($teamcomplete) && ($teamcomplete->getRound()->getId() == $round->getId())): ?>
                <div class="alert alert-danger">
                  <?php $playersnotpayed = $teamcomplete->getPlayersNotPayed() ?>
                  <?php if (count($playersnotpayed) > 0): ?>
                    ATTENTION: Toutes les places de cette équipe sont réservées mais certaines places ne sont pas payées.
                    <br/>
                    Cliquez sur le joueur que vous souhaitez supprimer afin de libèrer une place:<br/>
                    <?php foreach ($playersnotpayed as $player): ?>
                      - <a class="player_del_one" href="#"
                           rel="<?php echo $player->getId() ?>"><?php echo $player->getsfGuardUser() ?></a><br/>
                    <?php endforeach; ?>
                    <script>
                      $(document).ready(function () {
                        $('.player_del_one').click(function () {
                          delPlayer($(this).attr('rel'));
                          return false;
                        });
                      });
                      function delPlayer(id) {
                        $.ajax({
                          url: "<?php echo url_for('@ajax_player_del') ?>",
                          type: "POST",
                          data: { player_id: id },
                          cache: false,
                          success: function (html) {
                            if (html == "1") {
                              $('#inputTeam<?php echo $round->getId() ?>').val("<?php echo $teamcomplete->getName(); ?>");
                              $('#form_team').submit();
                            } else {
                              alert('Une erreur est survenue (merci d\'actualiser la page puis réesayer).');
                              window.location.reload();
                            }
                          },
                          error: function () {
                            alert('Une erreur est survenue (merci d\'actualiser la page puis réesayer).');
                            window.location.reload();
                          }
                        });
                      }
                    </script>
                  <?php else: ?>
                    Désolé, toutes les places de cette équipe sont réservées et payées, vous ne pouvez donc pas vous y inscrire.
                  <?php endif; ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <div class="form-actions form-actions-right" style="margin-bottom: 0;">
            <input type="hidden" name="round" value="<?php echo $round->getId() ?>">
            <input type="submit" class="btn btn-primary btn-submit" value="Etape Suivante">
          </div>
        </form>
      </div>
    </div>
  <?php endforeach; ?>

  <script type="text/javascript">
    $(document).ready(function () {
      $('form').each(function () {
        $(this).validate({
          errorElement: "span",
          errorClass: "help-inline error",
          validClass: "help-inline valid",
        });
      });
      $('.reveal_choice_parent_hidden').click(function () {
        if ($(this).hasClass('reveal_choice_parent_hidden')) {
          $('.toreveal').hide();
          $('.reveal_choice_parent').removeClass('reveal_choice_parent_hidden').addClass('reveal_choice_parent_hidden');
          $(this).removeClass('reveal_choice_parent_hidden').children('.toreveal').show();
        }
      });
    });
  </script>
<?php endif; ?>
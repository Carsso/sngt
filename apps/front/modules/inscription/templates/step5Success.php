<div class="boxed">
  <?php include_partial('inscription/steps', array('inscrstep' => $inscrstep)) ?>
</div>
<div class="boxed">
  <h2>Confirmation de votre inscription à l'étape de <?php echo $step ?></h2>

  <p>
    Votre paiement à bien été validé
  </p>

  <h2>Information Importante</h2>
  <p class="big">
    Pensez à vous munir de votre pièce d'identité, elle est obligatoire pour votre participation.
  </p>

  <p>
    ATTENTION: Les mineurs doivent impérativement être accompagné d'une personne majeure pendant toute la durée de l'évènement.<br />
    Si vous êtes mineur, vous devez fournir une <a href="http://sngt.fr/uploads/decharge2013.pdf" target="_blank">décharge de responsabilité</a> remplie par votre responsable légal ainsi qu'une photocopie de sa pièce d'identité.</a>
  </p>

  <h2>Billet électronique</h2>
  <p class="big">
    Pour un traitement plus rapide lors de votre arrivée, pensez à imprimer votre billet électronique.<br />
    Vous pouvez également noter votre numéro de billet: <?php echo $player->getYurplanToken() ?><br />
    <small>Votre billet electronique à été envoyé sur votre adresse email par YurPlan</small>
  </p>


  <div class="form-actions actions sf_admin_actions form-actions-right" style="margin-bottom: 0;">
    <a href="<?php echo url_for('etape', $step) ?>" class="btn btn-primary btn-submit btn-large">Retourner à la page de l'étape de <?php echo $step ?></a>
  </div>

</div>
<?php

/**
 * participation actions.
 *
 * @package    SNGT
 * @subpackage participation
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class inscriptionActions extends sfActions
{
  public function chkInscr($step)
  {
    if ($this->getUser()->isAuthenticated()) {
      $player = Doctrine_Query::create()
          ->from('Player p')
          ->leftJoin('p.Team t')
          ->leftJoin('t.Round r')
          ->where('r.step_id = ?', $step->getId())
          ->addWhere('p.player_id  = ?', $this->getUser()->getGuardUser()->getId())
          ->addWhere('p.is_payed  = 1')
          ->fetchOne();
      if (is_object($player)) {
        return $this->redirect('inscription_step_already', array('step_slug' => $step->getSlug()));
        die();
      }
    }

  }

  public function redirToStep($step, $chk = true)
  {
    $step_id = $this->getUser()->getAttribute('step', 1);
    if ($chk) {
      $this->chkInscr($step);
    }
    if ($this->inscrstep != $step_id) {
      return $this->redirect('inscription_step' . $step_id, array('step_slug' => $step->getSlug()));
      die();
    }
  }

  public function executeDelPlayer(sfWebRequest $request)
  {
    sfConfig::set('sf_web_debug', false);
    if ($request->isMethod('post')) {
      $player_id = $request->getPostParameter('player_id', 0);
      $team_id = $this->getUser()->getAttribute('teamcomplete');
      $team = Doctrine_Core::getTable('Team')->find($team_id);
      $this->forward404Unless($team);
      $playersnotpayed = $team->getPlayersNotPayed();
      foreach ($playersnotpayed as $one_player) {
        if ($one_player->getId() == $player_id) {
          $one_player->delete();
          return $this->renderText(1);
        }
      }
      return $this->renderText(-1);
    }
    return $this->renderText(0);
  }

  public function executeGetPlayers(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    $name = $request->getParameter('team_name', null);
    $round = $request->getParameter('round_id', null);
    $this->forward404Unless($name && $round && !empty($name) && !empty($round));
    $team = Doctrine_Query::create()
        ->from('Team')
        ->where('round_id = ?', $round)
        ->addWhere('LOWER(name) LIKE ?', strtolower($name))
        ->fetchOne();
    $this->forward404Unless(is_object($team));
    $players_array = array();
    foreach ($team->getPlayer() as $player) {
      if ($player->getIsPayed()) {
        $players_array[] = $player->getUser()->__toString();
      }
    }
    return $this->renderText(json_encode($players_array));
  }

  public
  function executeListAjax(sfWebRequest $request)
  {
    $step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($step);
    $json_txt = array();
    $yurplan = new Yurplan();
    foreach ($step->getRound() as $round) {
      $resultyurplan = $yurplan->listTickets($round->getYurplanEventId());
      if ($resultyurplan) {
        $json_txt[$round->getId()] = $round . ' : ' . "\r\n" . $resultyurplan;
      } else {
        $json_txt[$round->getId()] = $round . ' : ' . "\r\n" . 'Empty YurPlan ID !';
      }
    }
    sfConfig::set('sf_web_debug', false);
    return $this->renderText(implode("\r\n\r\n", $json_txt));
  }

  public
  function executeCheckAjax(sfWebRequest $request)
  {
    $player_id = $request->getParameter('player_id', null);
    $this->forward404Unless($player_id);
    $yurplan = new Yurplan();
    $player = Doctrine_Core::getTable('Player')->find($player_id);
    sfConfig::set('sf_web_debug', false);
    if (is_object($player)) {
      $yurplan->updatePayment($player->getTeam()->getRound()->getYurplanEventId(), $player);
      return $this->renderText((int)$player->getIsPayed());
    } else {
      return $this->renderText(-1);
    }
  }

  public
  function executeCheckAllAjax(sfWebRequest $request)
  {
    $step_id = $this->getUser()->getAttribute('step_id', null);
    $query = Doctrine_Query::create()
        ->from('Player p')
        ->leftJoin('p.Team t')
        ->leftJoin('t.Round r')
        ->leftJoin('r.Step s')
        ->where('p.is_payed IS NULL OR p.is_payed = 0');
    if ($step_id) {
      $query = $query->addWhere('s.id = ?', $step_id);
    }
    $players = $query->execute();
    $result = array();
    $yurplan = new Yurplan();
    foreach ($players as $player) {
      $yurplan->updatePayment($player->getTeam()->getRound()->getYurplanEventId(), $player);
      $result[] = array(
        'user_id' => $player->getSfGuardUser()->getId(),
        'user' => $player->getSfGuardUser()->__toString(),
        'player_id' => $player->getId(),
        'is_payed' => (int)$player->getIsPayed()
      );

    }
    sfConfig::set('sf_web_debug', false);
    return $this->renderText(json_encode($result));
  }

  public
  function executeStepAlready(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 0;

    if ($this->getUser()->isAuthenticated()) {
      $player = Doctrine_Query::create()
          ->from('Player p')
          ->leftJoin('p.Team t')
          ->leftJoin('t.Round r')
          ->where('r.step_id = ?', $this->step->getId())
          ->addWhere('p.player_id  = ?', $this->getUser()->getGuardUser()->getId())
          ->addWhere('p.is_payed  = 1')
          ->fetchOne();
      if (is_object($player)) {
        $this->player = $player;
      }
    }
  }
  public
  function executeStepComplete(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 0;
  }

  public
  function executeStep1(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 1;
    $this->chkInscr($this->step);
    if ($request->isMethod('post')) {
      $this->getUser()->setAttribute('step', 2);
      $this->redirToStep($this->step);
    } else {
      if ($this->getUser()->isAuthenticated()) {
        $players = Doctrine_Query::create()
            ->from('Player p')
            ->leftJoin('p.Team t')
            ->leftJoin('t.Round r')
            ->where('r.step_id = ?', $this->step->getId())
            ->addWhere('p.player_id  = ?', $this->getUser()->getGuardUser()->getId())
            ->addWhere('p.is_payed  = 0 OR p.is_payed IS NULL')
            ->execute();
        foreach ($players as $player) {
          $player->delete();
        }
      }
      $this->getUser()->setAttribute('team', null);
      $this->getUser()->setAttribute('player', null);
      $this->getUser()->setAttribute('step', 1);
    }
  }

  public
  function executeStep2(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 2;
    $this->redirToStep($this->step);
    $this->teamcomplete = false;
    if ($request->isMethod('post')) {
      $name = $request->getParameter('team', null);
      $round_id = $request->getParameter('round', null);
      $round = Doctrine_Core::getTable('Round')->find($round_id);
      $this->forward404Unless(is_object($round));
      if ($name && $round_id && !empty($name) && !empty($round_id)) {
        $team = Doctrine_Query::create()
            ->from('Team')
            ->where('round_id = ?', $round_id)
            ->addWhere('LOWER(name) LIKE ?', strtolower($name))
            ->fetchOne();
        if (is_object($team)) {
          if($round->getIsComplete() && !$team->getIsReserved()){
            $this->forward('inscription','stepComplete');
          }
          if ($team->getIsComplete()) {
            $this->getUser()->setAttribute('teamcomplete', $team->getId());
            $this->teamcomplete = $team;
          } else {
            $this->getUser()->setAttribute('team', $team->getId());
            $this->getUser()->setAttribute('step', 3);
            $this->redirToStep($this->step);
          }
        } else {
          if($round->getIsComplete()){
            $this->forward('inscription','stepComplete');
          }
          $team = new Team();
          $team->setName($name)
              ->setRoundId($round_id)
              ->save();
          $this->getUser()->setAttribute('team', $team->getId());
          $this->getUser()->setAttribute('step', 3);
          $this->redirToStep($this->step);
        }
      }
    } else {
      if ($this->getUser()->isAuthenticated()) {
        $players = Doctrine_Query::create()
            ->from('Player p')
            ->leftJoin('p.Team t')
            ->leftJoin('t.Round r')
            ->where('r.step_id = ?', $this->step->getId())
            ->addWhere('p.player_id  = ?', $this->getUser()->getGuardUser()->getId())
            ->addWhere('p.is_payed  = 0 OR p.is_payed IS NULL')
            ->execute();
        foreach ($players as $player) {
          $player->delete();
        }
      }
    }
  }

  public
  function executeStep3(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 3;
    $this->redirToStep($this->step);
    $team_id = $this->getUser()->getAttribute('team', null);
    $this->team = Doctrine_Core::getTable('Team')->find($team_id);
    $this->forward404Unless($this->team);
    $round = $this->team->getRound();
    if($round->getIsComplete() && !$this->team->getIsReserved()){
      $this->forward('inscription','stepComplete');
    }
    $this->reglement = Doctrine_Core::getTable('Cms')->findOneBy('slug', 'reglement');
    if ($request->isMethod('post')) {
      if ($request->hasParameter('reglement', false)) {
        $player = new Player();
        $player->setSfGuardUser($this->getUser()->getGuardUser())
            ->setTeamId($team_id)
            ->save();
        $this->getUser()->setAttribute('player', $player->getId());
        $this->getUser()->setAttribute('step', 4);
        $this->redirToStep($this->step);
      }
    } else {
    }
  }

  public
  function executeStep4(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 4;
    $this->redirToStep($this->step, false);
    $this->player_id = $this->getUser()->getAttribute('player', null);
    $this->forward404Unless($this->player_id);
    $this->player = Doctrine_Core::getTable('Player')->find($this->player_id);
    $this->forward404Unless($this->player);
    $this->team = $this->player->getTeam();
    $this->round = $this->team->getRound();
    $this->guard = $this->player->getSfGuardUser();
    $this->profile = $this->guard->getProfile();
    $this->yurplanurl = sfConfig::get('app_yurplan_url');
    if($this->round->getIsComplete() && !$this->team->getIsReserved()){
      $this->forward('inscription','stepComplete');
    }
    if ($this->player->getIsPayed()) {
      $this->getUser()->setAttribute('step', 5);
      $this->redirToStep($this->step, false);
    }
  }

  public
  function executeStep5(sfWebRequest $request)
  {
    $this->step = Doctrine_Core::getTable('Step')->findOneBy('slug', $request->getParameter('step_slug', ''));
    $this->forward404Unless($this->step);
    $this->inscrstep = 5;
    $this->player_id = $this->getUser()->getAttribute('player', null);
    $this->forward404Unless($this->player_id);
    $this->player = Doctrine_Core::getTable('Player')->find($this->player_id);
    if (!$this->player->getIsPayed()) {
      $this->redirToStep($this->step, false);
    }
  }

}

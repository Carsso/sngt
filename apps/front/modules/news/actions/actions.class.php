<?php

/**
 * news actions.
 *
 * @package    SNGT
 * @subpackage news
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->splash = Doctrine_Core::getTable('Cms')->findOneBy('slug', 'splash');
    if ($this->splash && !$this->getUser()->getAttribute('has_seen_splash', false)) {
      $this->getUser()->setAttribute('has_seen_splash', true);
      $this->forward('index', 'splash');
    }
    $this->newss = Doctrine_Query::create()
        ->from('News')
        ->where('is_visible =  1')
        ->orderBy('updated_at DESC')
        ->limit(10)
        ->execute();
    $this->topnewss = Doctrine_Query::create()
        ->from('TopNews')
        ->orderBy('updated_at DESC')
        ->limit(10)
        ->execute();
    $this->the_next_step = Doctrine_Query::create()
        ->from('Step')
        ->where('start_date > NOW()')
        ->addWhere('display_on_list = 1')
        ->orderBy('start_date ASC')
        ->fetchOne();
    $this->the_current_step = Doctrine_Query::create()
        ->from('Step')
        ->where('start_date < NOW() AND end_date > NOW()')
        ->addWhere('display_on_list = 1')
        ->orderBy('end_date ASC')
        ->fetchOne();
    $this->stream = Doctrine_Query::create()
        ->from('Stream')
        ->orderBy('elem_order, name')
        ->fetchOne();
    $this->steps = Doctrine_Query::create()
        ->from('Step')
        ->where('display_on_list = 1')
        ->orderBy('start_date ASC')
        ->execute();

    $this->route = $this->getContext()->getRouting()->getCurrentRouteName();
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->news = $this->getRoute()->getObject();
  }
}

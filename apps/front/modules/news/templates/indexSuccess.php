<div class="right_col column">
  <div id="inscriptions"
       style="<?php if ($the_next_step && $the_next_step->getImage()): ?>background-image: url(<?php echo image_path($the_next_step->getImage()) ?>)<?php endif; ?>">
    <?php if ($sf_user->isAuthenticated()): ?>
      <?php include_partial('index/mesinscriptions', array('the_next_step' => $the_next_step)) ?>
    <?php else: ?>
      <div class="p_center boxed"
          <?php if ($the_next_step): ?>
        onclick="$(location).attr('href','<?php echo url_for('etape', $the_next_step) ?>');"
        style="cursor:pointer;"
      <?php endif; ?>
          >
        <h2>Prochaine étape</h2>
        <?php if ($the_next_step): ?>
          <p class="city">
            <?php echo $the_next_step->getCity() ?>
          </p>
          <p class="date">
            <?php echo $the_next_step->getFormattedDate() ?>
          </p>
          <p style="margin-top:15px;">
            <a href="<?php echo url_for('etape', $the_next_step) ?>"
               class="btn btn-sngt btn-large">Details <?php echo ($the_next_step->getStatus() == 1) ? 'et Inscription' : ''; ?></a>
          </p>
        <?php else: ?>
          <p class="big">
            Aucun évènement n'est planifié actuellement
          </p>
          <p style="margin-top:15px;">
            Une question, un problème ?<br/>N'hésitez pas à nous contacter !
          </p>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  </div>
  <div id="stepsmap" class="p_center boxed">
    <h2>Les étapes</h2>

    <div style="width: 320px; height: 320px; display: block; position: relative; cursor: pointer;"
         onclick="document.location.href='<?php echo url_for('etapes') ?>';">
      <div
          style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 100;cursor: pointer !important;"></div>
      <div id="map_canvas" style="width: 310px; height: 320px;"></div>
    </div>
    <script type="text/javascript">
      var styles = [
        {
          featureType: 'water',
          elementType: 'all',
          stylers: [
            { hue: '#172240' },
            { saturation: 4 },
            { lightness: -78 },
            { visibility: 'simplified' }
          ]
        },
        {
          featureType: 'poi',
          elementType: 'all',
          stylers: [
            { hue: '#30384f' },
            { saturation: -43 },
            { lightness: -68 },
            { visibility: 'off' }
          ]
        },
        {
          featureType: 'administrative',
          elementType: 'all',
          stylers: [
            { hue: '#62697b' },
            { saturation: 11 },
            { lightness: -15 },
            { visibility: 'simplified' }
          ]
        },
        {
          featureType: 'landscape',
          elementType: 'all',
          stylers: [
            { hue: '#212c48' },
            { saturation: 14 },
            { lightness: -77 },
            { visibility: 'on' }
          ]
        },
        {
          featureType: 'road',
          elementType: 'all',
          stylers: [
            { hue: '#c1c4ce' },
            { saturation: -88 },
            { lightness: 40 },
            { visibility: 'on' }
          ]
        }
      ];
    </script>
    <?php include_partial('steps/listmap', array('steps' => $steps, 'zoom' => 5, 'lock' => true)) ?>
  </div>
</div>
<div class="left_col column">
  <div id="slide" class="p_center">
    <?php if ($the_current_step): ?>
      <div id="realtime">
        <?php if ($stream): ?>
          <div id="stream" class="float_right">
            <iframe src="<?php echo $stream->getUrl() ?>" width="420" height="250" frameborder="0"></iframe>
          </div>
        <?php else: ?>
          <div class="float_right"
               style="width: 420px;height: 250px;border: 1px solid #111;background: #222;color:#ccc">
            <p style="margin-top:100px">
              Aucun flux vidéo en direct n'est disponible actuellement...
            </p>
          </div>
        <?php endif; ?>
        <div id="realtimestep">
          <h2>Étape en cours</h2>

          <p class="big" style="margin-bottom:13px;margin-top: 30px;">
            <?php echo $the_current_step->getCity() ?>
          </p>

          <p>
            <a href="" class="btn btn-warning btn-large">Resultats en temps réel</a>
          </p>
          <p>
            <a href="<?php echo url_for('@streams') ?>" class="btn btn-danger btn-large">Flux vidéo en direct</a>
          </p>
        </div>
      </div>
    <?php else: ?>
      <div id="slider_nivo_parent">
        <?php if (count($topnewss) > 0): ?>
          <div id="slider" class="nivoSlider theme-default">
            <?php foreach ($topnewss as $news): ?>
              <?php echo image_tag($news->getImage(), array('title' => $news->getText())) ?>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  </div>
  <div id="lastnews" class="boxed" style="padding-bottom: 15px;">
    <h2>News</h2>
    <?php $first_news = true; ?>
    <?php  ?>
    <?php foreach ($newss as $news): ?>
      <?php
      $news_string = strip_tags(majaxMarkdown::transform(sfOutputEscaper::unescape($news->getContent())));
      $minword = 4;
      $length = 380;
      $sub = '';
      $len = 0;
      foreach (explode(' ', $news_string) as $word) {
        $part = (($sub != '') ? ' ' : '') . $word;
        $sub .= $part;
        $len += strlen($part);

        if (strlen($word) > $minword && strlen($sub) >= $length) {
          break;
        }
      }

      $stringDisplay = $sub . (($len < strlen($news_string)) ? '...' : '');
      $stringDisplay = str_replace("\n", ' ', $stringDisplay);
      $stringDisplay = str_replace('&nbsp;', ' ', $stringDisplay);
      for ($i = 0; $i < 4; $i++) {
        $stringDisplay = str_replace('  ', ' ', $stringDisplay);
      }
      ?>
      <div class="span7 newsp">
        <div class="caldate span1">
          <div class="calday"><?php echo format_date($news->getUpdatedAt(), 'dd', 'fr') ?></div>
          <div class="calmonth"><?php echo format_date($news->getUpdatedAt(), 'MMM', 'fr') ?></div>
        </div>
        <div class="newsi">

          <?php
          if ($first_news):
            ?>
            <h3>
              <a href="<?php echo url_for('news_show', $news) ?>" class="newstitle"><?php echo $news->getTitle() ?></a>
            </h3>
            <p style="text-align: justify;margin:0;margin-bottom: 5px;">
              <span class="newspreview"><?php echo $stringDisplay ?></span>
            <span style="text-align: right; width: 100px; display:block; float:right;">
              <a href="<?php echo url_for('news_show', $news) ?>">Lire la suite</a>
            </span>
            </p>
          <?php else:
            ?>
            <h3 style="margin-top: 15px;">
              <a href="<?php echo url_for('news_show', $news) ?>" class="newstitle"><?php echo $news->getTitle() ?></a>
            </h3>
          <?php
          endif;
          ?>
        </div>
      </div>
      <div class="clear"></div>
      <hr/>
      <?php $first_news = false; ?>
    <?php endforeach; ?>
  </div>
</div>
<div class="clear"></div>
<script type="text/javascript">

  var doBlinkBg = function (obj, start, finish) {
    jQuery(obj).animate({ backgroundColor: "#0c4a7d" }, 300).animate({ backgroundColor: "#172240" }, 300);
    if (start != finish) {
      start = start + 1;
      doBlinkBg(obj, start, finish);
    }
  }
  var doBlink = function (obj, start, finish) {
    jQuery(obj).fadeOut(300).fadeIn(300);
    if (start != finish) {
      start = start + 1;
      doBlink(obj, start, finish);
    }
  }
  $(document).ready(function () {
    $('#slider').nivoSlider();
    <?php if($route == 'mes_inscriptions'): ?>
    doBlink("#inscriptions", 1, 4);
    <?php endif; ?>
  });
</script>

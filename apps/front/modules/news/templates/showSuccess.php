<div class="newsc" style="min-height:200px;">
  <div class="titlenews"><?php echo $news->getTitle() ?></div>

  <div class="caldatebig span1">
    <div class="calday"><?php echo format_date($news->getUpdatedAt(), 'dd', 'fr') ?></div>
    <div class="calmonth"><?php echo format_date($news->getUpdatedAt(), 'MMM', 'fr') ?></div>
  </div>
  <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($news->getContent()), false, false) ?>
</div>
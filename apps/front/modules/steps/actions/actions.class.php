<?php

/**
 * steps actions.
 *
 * @package    SNGT
 * @subpackage steps
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class stepsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->the_next_step = Doctrine_Query::create()
        ->from('Step')
        ->where('start_date > NOW()')
        ->addWhere('display_on_list = 1')
        ->orderBy('start_date ASC')
        ->fetchOne();
    $this->the_current_step = Doctrine_Query::create()
        ->from('Step')
        ->where('start_date < NOW() AND end_date > NOW()')
        ->addWhere('display_on_list = 1')
        ->orderBy('end_date ASC')
        ->fetchOne();
    $this->steps = Doctrine_Query::create()
        ->from('Step')
        ->where('display_on_list = 1')
        ->orderBy('start_date ASC')
        ->execute();
  }

  public function executeIcs(sfWebRequest $request)
  {
    $steps = Doctrine_Query::create()
        ->from('Step')
        ->where('display_on_list = 1')
        ->orderBy('start_date ASC')
        ->execute();

    $ics = "BEGIN:VCALENDAR\n";
    $ics .= "PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\n";
    $ics .= "VERSION:2.0\n";
    $ics .= "METHOD:PUBLISH\n";
    $ics .= "X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n";
    foreach($steps as $step){
      $endofevt = strtotime($step->getEndDate());
      $startofevt = strtotime($step->getStartDate());
      $titleevt = $step->__toString();
      $placeofevt = str_replace(array("\r\n","\r","\n"),' ',$step->getContentMap());
      $ics .= "BEGIN:VEVENT\n";
      $ics .= "CLASS:PUBLIC\n";
      $ics .= "CREATED:20091109T101015Z\n";
      $ics .= "DESCRIPTION:SNGT ".$titleevt.'\n'.$this->generateUrl('etape',$step, true)."\n";
      $ics .= "DTEND:" . gmdate('Ymd',$endofevt).'T'. gmdate('His',$endofevt) . "Z\n";
      $ics .= "DTSTAMP:" . gmdate('Ymd').'T'. gmdate('His') . "Z\n";
      $ics .= "DTSTART:" . gmdate('Ymd',$startofevt).'T'. gmdate('His',$startofevt) . "Z\n";
      $ics .= "LAST-MODIFIED:" . gmdate('Ymd').'T'. gmdate('His') . "Z\n";
      $ics .= "LOCATION:".$placeofevt."\n";
      $ics .= "PRIORITY:5\n";
      $ics .= "SEQUENCE:0\n";
      $ics .= "SUMMARY:SNGT ".$titleevt."\n";
      $ics .= "TRANSP:OPAQUE\n";
      $ics .= "UID:" . md5(uniqid(mt_rand(), true)."@SNGT")."\n";
      $ics .= "X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n";
      $ics .= "X-MICROSOFT-CDO-IMPORTANCE:1\n";
      $ics .= "X-MICROSOFT-DISALLOW-COUNTER:FALSE\n";
      $ics .= "X-MS-OLK-ALLOWEXTERNCHECK:TRUE\n";
      $ics .= "X-MS-OLK-AUTOFILLLOCATION:FALSE\n";
      $ics .= "X-MS-OLK-CONFTYPE:0\n";
      //Here is to set the reminder for the event.
      $ics .= "BEGIN:VALARM\n";
      $ics .= "TRIGGER:-PT1440M\n";
      $ics .= "ACTION:DISPLAY\n";
      $ics .= "DESCRIPTION:SNGT ".$titleevt.'\n'.$this->generateUrl('etape',$step, true)."\n";
      $ics .= "END:VALARM\n";
      $ics .= "END:VEVENT\n";
    }
    $ics .= "END:VCALENDAR\n";

    $this->setLayout(false);
    sfConfig::set('sf_web_debug', false);
    $response = $this->getResponse();
    $response->clearHttpHeaders();
    $response->setStatusCode(200);
    $response->setContentType('text/Calendar');
    $response->setHttpHeader('Expires', $this->getContext()->getResponse() -> getDate(time()), true);
    $response->setHttpHeader('Last-Modified', gmdate('D, d M Y H:i:s') . ' GMT', true);
    $response->setHttpHeader('Cache-Control', 'no-cache', true);
    $response->setContent($ics);
    return sfView::NONE;
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->step = $this->getRoute()->getObject();
  }

  public function executeList(sfWebRequest $request)
  {
    $this->step = $this->getRoute()->getObject();
    $participation = array();
    $rounds = Doctrine_Query::create()
        ->from('Round r')
        ->leftJoin('r.Team t')
        ->leftJoin('t.Player p')
        ->where('r.step_id = ?', $this->step->getId())
        ->execute();
    foreach($rounds as $round){
      $teams = $round->getTeam();
      $array_participation = array();
      $array_participation['round']=$round;
      $array_participation['teams']=$teams;
      $array_participation['teams_nb']=count($teams);
      $nbteams = 0;
      foreach($teams as $team){
        if($team->getIsReserved()){
          $nbteams++;
        }
      }
      $array_participation['teams_reserved_nb']=$nbteams;
      $participation[$round->getId()]=$array_participation;
    }
    $this->participations = $participation;
  }

  public function executeChart(sfWebRequest $request)
  {
    $this->round = $this->getRoute()->getObject();
    $this->step = $this->round->getStep();
    $this->game = $this->round->getGame();
    $this->stats = $this->round->getStatsInscriptions();
  }
  public function executeRedirEtapes(sfWebRequest $request)
  {
    return $this->redirect($this->generateUrl('etapes'),301);
  }
}

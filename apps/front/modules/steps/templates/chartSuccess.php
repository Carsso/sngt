<div class="boxed">
  <h2>Evolution du nombre d'inscriptions d'équipes - <?php echo $step->getName() ?>
    - <?php echo $game->getName() ?></h2>
  <table id="data">
    <tfoot>
    <tr>
      <?php foreach ($stats as $key => $val): ?>
        <th><?php echo $key ?></th>
      <?php endforeach; ?>
    </tr>
    </tfoot>
    <tbody>
    <tr>
      <?php foreach ($stats as $key => $val): ?>
        <td><?php echo $val ?></td>
      <?php endforeach; ?>
    </tr>
    </tbody>
  </table>
  <div id="holder" style="height: 400px; width: 940px; margin:20px;"></div>
</div>
<?php if ($step->getStatus() == 1): ?>
  <p style="text-align: center; padding-bottom: 40px; margin-top:20px;">
    <a href="<?php echo url_for('participation_choix_jeu', $step) ?>" class="btn btn-primary btn-larger">Inscription</a>
  </p>
<?php endif; ?>
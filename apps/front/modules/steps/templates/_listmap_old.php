<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<script type="text/javascript">
  var map;
  var geocoder;
  var markers = [];
  var infowindows = [];
  var fr_lat_lng = new google.maps.LatLng(46.8114340, 1.6867790);
  function initialize() {
    geocoder = new google.maps.Geocoder();
    <?php if(!$lock): ?>
    var mapOptions = {
      zoom: <?php echo $zoom ?>,
      center: fr_lat_lng,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      draggable: false,
      streetViewControl: false
    }
    <?php else: ?>
    var mapOptions = {
      zoom: <?php echo $zoom ?>,
      center: fr_lat_lng,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      mapTypeControl: false,
      streetViewControl: false
    }
    <?php endif; ?>
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    <?php foreach($steps as $step): ?>
    geocoder.geocode({ 'address': '<?php echo str_replace(array("\n","\r"),' ',$step->getContentMap()) ?>'}, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var infowindowcontent = '';
        if (typeof markers['<?php echo strtolower($step->getCity()) ?>'] != 'undefined') {
          infowindowcontent = infowindows['<?php echo strtolower($step->getCity()) ?>'].getContent() + '<hr />';
          google.maps.event.clearListeners(markers['<?php echo strtolower($step->getCity()) ?>'], 'click');
        }
        markers['<?php echo strtolower($step->getCity()) ?>'] = new google.maps.Marker({
          position: results[0].geometry.location,
          map: map,
          title: '<?php echo $step->getCity() ?>'
        });
        <?php if($lock): ?>
        infowindows['<?php echo strtolower($step->getCity()) ?>'] = new google.maps.InfoWindow({
          content: infowindowcontent + '<div style="cursor:pointer;" onclick="window.location=\'<?php echo url_for('etape',$step) ?>\';"><h2 style="margin-left:0; margin-top: 0; margin-bottom:8px;"><?php echo $step->getCity() ?></h2><p style="margin-bottom:8px;text-align:left;"><?php echo $step->getFormattedDate() ?></p><p style="margin-bottom:8px;text-align:left;"><?php if($step->getStatus()): ?><span class="label label-success">Inscriptions ouvertes</span><?php else: ?><span class="label label-important">Inscriptions fermées</span><?php endif; ?></p></div>',
        });
        google.maps.event.addListener(markers['<?php echo strtolower($step->getCity()) ?>'], 'click', function () {
          infowindows['<?php echo strtolower($step->getCity()) ?>'].open(map, markers['<?php echo strtolower($step->getCity()) ?>']);
        });
        <?php endif; ?>
      } else {
      }
    });
    <?php endforeach; ?>
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php use_javascript('https://maps.google.com/maps/api/js?sensor=false') ?>
<?php use_javascript('mxn/mxn.js?(googlev3,[geocoder])') ?>


<?php
$infowindows = array();
$steps_city = array();
?>
<?php
foreach ($steps as $step):
  $steps_city[strtolower($step->getCity())] = $step;
  if (!isset($infowindows[strtolower($step->getCity())])) {
    $infowindows[strtolower($step->getCity())] = '';
  } else {
    $infowindows[strtolower($step->getCity())] .= '<hr />';
  }
  $content_info = addcslashes(str_replace(array("\r","\n"),'',get_partial('steps/elem_map', array('step'=>$step, 'btn'=>false))),'\'');
  $infowindows[strtolower($step->getCity())] .= $content_info;
endforeach;
?>

<script type="text/javascript">
  var map;
  var markers = [];
  var infowindows = [];
  var fr_lat_lng;
  if(typeof styles == "undefined"){
    styles = null;
  }
  $(document).ready(function () {
    map = new mxn.Mapstraction('map_canvas', 'googlev3', true);
    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
    fr_lat_lng = new mxn.LatLonPoint(46.8114340, 1.6867790)
    map.setCenterAndZoom(fr_lat_lng, <?php echo $zoom ?>);
    map.setMapType(mxn.Mapstraction.PHYSICAL);
    var googlemap = map.getMap();
    googlemap.mapTypes.set('map_style', styledMap);
    googlemap.setMapTypeId('map_style');
    map.addControls({
      <?php if($lock): ?>
      pan: false,
      overview: false,
      map_type: false,
      <?php else: ?>
      pan: true,
      overview: false,
      map_type: false,
      zoom: 'large',
      <?php endif; ?>
    });
    <?php foreach($steps_city as $step): ?>
    var geocoder = new mxn.Geocoder('googlev3', addMarker<?php echo ucfirst($step->getCity()) ?>);
    var address = new Object();
    address.street = '<?php echo str_replace(array("\n","\r"),' ',$step->getContentMap()) ?>';
    geocoder.geocode(address);
    <?php endforeach; ?>
  });
  <?php foreach($steps_city as $step): ?>
  function addMarker<?php echo ucfirst($step->getCity()) ?>(waypoint) {
    markers['<?php echo strtolower($step->getCity()) ?>'] = new mxn.Marker(waypoint.point)
    markers['<?php echo strtolower($step->getCity()) ?>'].setInfoBubble('<?php echo $infowindows[strtolower($step->getCity())] ?>');
    map.addMarker(markers['<?php echo strtolower($step->getCity()) ?>']);
  }
  <?php endforeach; ?>
</script>
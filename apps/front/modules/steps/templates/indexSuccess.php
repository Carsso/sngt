<?php if ($the_next_step): ?>
  <div class="boxed lightblue all_col next_step_all">
    <div class="float_right">
      <p style="margin-top:6px;margin-bottom:0px;">
        <a href="<?php echo url_for('etape', $the_next_step) ?>"
           class="btn btn-primary btn-larger">Details <?php echo ($the_next_step->getStatus() == 1) ? 'et Inscription' : ''; ?></a>
      </p>
    </div>
    <div id="realtimestep">
      <h2 style="margin-bottom:5px;">Prochaine étape</h2>

      <p class="big" style="margin-left:20px;margin-bottom:5px;">
        <?php echo $the_next_step->getCity() ?> (<?php echo $the_next_step->getFormattedDate() ?>)
      </p>
    </div>
  </div>
<?php endif; ?>
<div class="right_col column">
  <div class="boxed">
    <?php
    $separator = false;
    foreach ($steps as $step):
      ?>
      <?php if ($separator): ?>
      <hr/>
    <?php endif; ?>
      <div class="">
        <?php echo get_partial('steps/elem_map', array('step' => $step, 'btn' => true)) ?>
      </div>
      <?php

      $separator = true;
    endforeach;
    ?>
  </div>
</div>
<div class="left_col column">
  <div>
    <div id="map_canvas" style="width: 660px; height: 800px;"></div>

    <script type="text/javascript">
      var styles = [
        {
          featureType: 'water',
          elementType: 'all',
          stylers: [
            { hue: '#0b1636' },
            { saturation: 38 },
            { lightness: -83 },
            { visibility: 'simplified' }
          ]
        },
        {
          featureType: 'poi',
          elementType: 'all',
          stylers: [
            { hue: '#30384f' },
            { saturation: -43 },
            { lightness: -68 },
            { visibility: 'off' }
          ]
        },
        {
          featureType: 'administrative',
          elementType: 'all',
          stylers: [
            { hue: '#62697b' },
            { saturation: 11 },
            { lightness: -15 },
            { visibility: 'simplified' }
          ]
        },
        {
          featureType: 'landscape',
          elementType: 'all',
          stylers: [
            { hue: '#212c48' },
            { saturation: 14 },
            { lightness: -77 },
            { visibility: 'on' }
          ]
        },
        {
          featureType: 'road',
          elementType: 'all',
          stylers: [
            { hue: '#c1c4ce' },
            { saturation: -88 },
            { lightness: 40 },
            { visibility: 'on' }
          ]
        }
      ];
    </script>
    <?php include_partial('steps/listmap', array('steps' => $steps, 'zoom' => 6, 'lock' => false)) ?>
  </div>
</div>
<script type="text/javascript">
  $('.tooltipify_steps').tooltip({
    placement: 'left'
  });
</script>
<div style="cursor:pointer;" onclick="window.location='<?php echo url_for('etape', $step) ?>';">
  <div style="float:left;margin: 0;width: 40px;">
    <?php $specific_style = ''; ?>
    <?php foreach ($step->getRound() as $round): ?>
      <?php echo image_tag($round->getGame()->getImage(), array('width' => '30px', 'height' => '30px', 'style' => 'vertical-align:top;' . $specific_style, 'class' => 'tooltipify_steps', 'title' => $round->getGame())) ?>
      <?php $specific_style = 'margin-top: 4px;'; ?>
    <?php endforeach; ?>
  </div>
  <div style="float:left;margin: 0;">
    <h2 style="padding-bottom: 3px;">
      <?php echo $step->getCity() ?>
    </h2>

    <p style="margin-bottom:3px;">
      <?php echo $step->getFormattedDate() ?>
    </p>

    <p style="margin-bottom: 2px;">
      <?php if ($sf_user->isAuthenticated()): ?>
        <?php if ($sf_user->getGuardUser()->isInscrit($step->getId())): ?>
          <span class="label label-inverse">Déjà inscrit</span>
        <?php else: ?>
          <?php if ($step->getStatus()): ?>
            <span class="label label-success">Inscriptions ouvertes</span>
          <?php else: ?>
            <span class="label label-important">Inscriptions fermées</span>
          <?php endif; ?>
        <?php endif; ?>
      <?php else: ?>
        <?php if ($step->getStatus()): ?>
          <span class="label label-success">Inscriptions ouvertes</span>
        <?php else: ?>
          <span class="label label-important">Inscriptions fermées</span>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($step->getIsGiga()): ?>
        <span class="label label-warning">Giga</span>
      <?php endif; ?>
    </p>
  </div>
  <?php if ($btn): ?>
    <div style="float:right;">
      <?php
      if ($step->getStatus()) {
        ?>
        <a href="<?php echo url_for('etape', $step) ?>" class="btn btn-success btn-large"
           style="padding: 10px 18px; margin: 0; line-height: 21px;">Détail de<br/>l'étape</a>
      <?php
      } else {
        ?>
        <a href="<?php echo url_for('etape', $step) ?>" class="btn btn-danger btn-large"
           style="padding: 10px 18px; margin: 0; line-height: 21px;">Détail de<br/>l'étape</a>
      <?php
      }
      ?>
    </div>
  <?php endif; ?>
  <div class="clear"></div>
</div>
<div class="right_col column">
  <div id="inscriptions" class="p_center step_inscr" style="height: 320px;border-bottom: 0px;<?php if($step->getImage()): ?>background-image: url(<?php echo image_path($step->getImage()) ?>)<?php endif; ?>">
    <h2 style="text-align: left;margin-top:10px;margin-left: 10px;">Étape de <?php echo $step->getCity() ?></h2>
    <div style="margin-top: 70px">
      <?php if ($sf_user->isAuthenticated() && $sf_user->getGuardUser()->isInscrit($step->getId())): // si il est connecté et inscrit ?>
        <div class="p_center" style="padding: 0 10px;">
          <p>
            Vous êtes inscrit à cette étape pour le jeu<br/>
            <strong><?php echo $sf_user->getGuardUser()->getInscription($step->getId())->getTeam()->getRound()->getGame() ?></strong>
            <br/>
            avec l'équipe:
            <br/>
            <a href="<?php echo url_for('@mes_inscriptions') ?>" class="big">
              <?php echo $sf_user->getGuardUser()->getInscription($step->getId())->getTeam() ?>
            </a>
          </p>
        </div>
      <?php elseif ($step->getStatus() == 1): // sinon si inscriptions ouvertes ?>
        <p style="margin-top: 20px;margin-bottom: 20px;">
          <a href="<?php echo url_for('inscription_step1', array('step_slug' => $step->getSlug())) ?>"
             class="btn btn-sngt" style="color:#ffffff;">Inscription<br />(<?php echo $step->getPrice() ?>€ par joueur)</a>
        </p>
      <?php endif; ?>
      <p style="margin-top: 10px;margin-bottom: 0">
        <a href="<?php echo url_for('etape_participants', $step) ?>" class="btn btn-sngt">Participants</a>
      </p>
      <?php if ($sf_user->isAuthenticated() && $sf_user->isSuperAdmin()): //si ADMIN ?>
        <div class="p_center" style="padding: 10px;margin-top:20px;">
          <p class="big">
            <a href="<?php echo url_for('inscription_list_ajx', array('step_slug' => $step->getSlug())) ?>"
               target="_blank">
              [ADMIN] Liste des tickets YurPlan</a>
          </p>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div id="map" class="boxed p_center step_plan">
    <iframe width="320" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
            src="https://maps.google.fr/maps?hl=fr&output=embed&q=<?php echo str_replace(array("\n", ' '), '+', $step->getContentMap()) ?>"></iframe>
  </div>
  <div class="p_center">
    <p>
      <a href="https://maps.google.fr/maps?hl=fr&q=<?php echo str_replace(array("\n", ' '), '+', $step->getContentMap()) ?>"
         target="_blank">Agrandir le plan</a>
    </p>

    <div style="margin: 0 5px 5px 5px;text-align: justify;">
      <p>
        <?php echo nl2br($step->getContentMap()) ?><br/>
        <a href="#" onclick="return false;" id="toggle_metro_bus" data-toggle="collapse" data-target="#step_transport">Infos
          Métro/Bus</a>
      </p>
    </div>
  </div>
  <div class="boxed step_transport collapse" id="step_transport" style="padding: 0 5px;">
    <div style="margin: 0 5px 5px 5px;text-align: justify;">
      <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($step->getContentAccess())) ?>
    </div>
  </div>
  <div class="boxed step_logements">
    <h2>Logements</h2>

    <div style="margin: 0 5px 5px 5px;text-align: justify;">
      <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($step->getContentHotel())) ?>
    </div>
  </div>
</div>
<div class="left_col column">
  <div class="step_description">
    <img src="<?php echo image_path('step_img') ?>"/>

    <div class="boxed" style="margin-top: 10px;">
      <h2>Étape de <?php echo $step ?></h2>

      <div style="text-align: justify;">
        <p>
          <?php if($step->getStatus()): ?><span
              class="label label-success">Inscriptions ouvertes</span><?php else: ?><span class="label label-important">Inscriptions fermées</span><?php endif; ?>
        </p>
        <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($step->getContent())) ?>
      </div>
    </div>
  </div>
  <div class="step_horaires boxed">
    <h2>Horaires</h2>

    <div style="margin: 0 5px 5px 5px;text-align: justify;">
      <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($step->getContentSchedule())) ?>
    </div>
  </div>
  <div class="boxed">
    <h2>Dotations</h2>

    <div style="margin: 0 5px 5px 5px;text-align: justify;">
      <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($step->getContentPrizes())) ?>
    </div>
  </div>
  <div class="boxed">
    <h2>Les jeux</h2>
    <?php foreach ($step->getRound() as $round): ?>
      <div style="width:64px;height:64px;float:left;margin: 0 10px;">
        <a href="<?php echo url_for('inscription_step1', array('step_slug' => $step->getSlug())) ?>">
          <?php echo image_tag($round->getGame()->getImage(), array('width' => '64px', 'height' => '64px')) ?>
        </a>
      </div>
      <div>
        <p>
          <a style="text-decoration: none;"
             href="<?php echo url_for('inscription_step1', array('step_slug' => $step->getSlug())) ?>">
            <strong><?php echo $round->getGame()->getName() ?></strong><br/>
            <?php echo $round->getGame()->getFormattedPlayers() ?>
          </a>
        </p>

        <div class="clear"></div>
        <hr/>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<div class="clear"></div>
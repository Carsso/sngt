<div class="boxed">
  <h2>Participants - <?php echo $step ?></h2>
  <?php foreach ($participations as $participation): ?>
    <?php $round = $participation['round'] ?>
    <?php $teams = $participation['teams'] ?>
    <div style="width:160px;float:left; text-align:center; margin: 0 10px;">
      <p>
        <a href="<?php echo url_for('inscription_step1', array('step_slug' => $step->getSlug())) ?>">
          <?php echo image_tag($round->getGame()->getImage(), array('width' => '150px', 'height' => '150px')) ?>
        </a>
      </p>

      <p>
        <a style="text-decoration: none;"
           href="<?php echo url_for('inscription_step1', array('step_slug' => $step->getSlug())) ?>">
          <strong><?php echo $round->getGame()->getName() ?></strong><br/>
          <?php echo $round->getNbTeamFree() ?> places restantes
        </a>
      </p>
    </div>
    <div style="width:760px;float:left;margin: 0 10px;">
      <p>
        <a href="#" class="btn participants_round<?php echo $round->getId() ?>_see"
           onclick="$('.participants_round<?php echo $round->getId() ?>_see').hide();$('.participants_round<?php echo $round->getId() ?>_hide').show();$('.participants_round<?php echo $round->getId() ?>').show('blind');return false;">
          <span><strong>Voir les participants</strong></span>
        </a>
        <a href="#" class="btn participants_round<?php echo $round->getId() ?>_hide"
           onclick="$('.participants_round<?php echo $round->getId() ?>_see').show();$('.participants_round<?php echo $round->getId() ?>_hide').hide();$('.participants_round<?php echo $round->getId() ?>').hide('blind');return false;"
           style="display:none;">
          <span><strong>Masquer les participants</strong></span>
        </a>
      </p>

      <div class="participants_round<?php echo $round->getId() ?>" style="display:none;">
        <?php foreach ($teams as $team): ?>
          <?php if(count($team->getPlayer())>0): ?>
            <p>
              <span title="<?php echo $team ?>"><strong><?php echo truncate_text($team, 20) ?>:</strong></span><br/>
              <?php foreach ($team->getPlayer() as $player): ?>
                <span title="<?php echo $player->getSfGuardUser()->getUsername() ?>">
                  <?php echo truncate_text('- ' . $player->getSfGuardUser()->getUsername(), 20) ?>
                    <?php
                    if ($player->getIsPayed()) {
                      echo '<span style="color:#008000;">Place Réservée</span>';
                    } else {
                      echo '<span style="color:#0074CC;">En attente</span>';
                    }
                    ?>
                </span><br/>
              <?php endforeach; ?>
            </p>
            <hr/>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <hr/>
  <?php endforeach; ?>
</div>
<?php if ($step->getStatus() == 1): ?>
  <p style="text-align: center; padding-bottom: 40px; margin-top:20px;">
    <a href="<?php echo url_for('inscription_step1', array('step_slug' => $step->getSlug())) ?>"
       class="btn btn-primary btn-larger">Inscription</a>
  </p>
<?php endif; ?>
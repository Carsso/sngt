<?php
class AutoLanguageFilter extends sfFilter
{
  public function execute($filterChain)
  {
    // Est-ce que la culture de l'utilisateur est incorrecte ?
    if ($this->getContext()->getUser()->getCulture() != 'fr') {
      $this->getContext()->getUser()->setCulture('fr');
    }

    // Execute next filter
    $filterChain->execute();
  }
}
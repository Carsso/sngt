<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <meta name="google-site-verification" content="btXvpz-_WyPBXfvIc5N30_LxIqQObMjVaoRK24GP8_4"/>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="<?php echo image_path('../favicon.ico') ?>"/>
  <?php include_stylesheets() ?>
  <?php include_javascripts() ?>
</head>
<body>
<div id="global">
  <?php
  if ($sf_user->hasFlash('info')) {
    echo '<div class="alert alert-info" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('info') . '</div>';
  }
  if ($sf_user->hasFlash('error')) {
    echo '<div class="alert alert-error" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('error') . '</div>';
  }
  if ($sf_user->hasFlash('success')) {
    echo '<div class="alert alert-success" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('success') . '</div>';
  }
  ?>
  <div id="header" class="clear" style="cursor:pointer;">
    <div class="content-inside">
      <div id="socials">
        <p class="marging">
          <a href="http://www.facebook.com/eventsngt" target="_blank">
            <button class="zocial facebook icon">Facebook</button>
          </a>
        </p>
        <p class="marging">
          <a href="https://twitter.com/EventSNGT" target="_blank">
            <button class="zocial twitter icon">Twitter</button>
          </a>
        </p>
        <p>
          <a href="<?php echo url_for('contact') ?>" id="call_number_link">
            <button class="zocial call icon">Numéro de téléphone</button>
          </a>
          <script type="text/javascript">
            $('#call_number_link').tooltip({
              title: 'Une question, un problème ? Contactez-nous par téléphone au 09.72.29.07.97',
              placement: 'left'
            });
          </script>
        </p>
      </div>
    </div>
  </div>
  <div class="navbar clear all_col">
    <div class="navbar-inner" style="border-radius: 0">
      <div class="content-inside">
        <div class="container">
          <div class="nav-collapse">
            <ul class="nav">
              <li>
                <a href="<?php echo url_for('@homepage') ?>">Accueil</a>
              </li>
              <li>
                <a href="<?php echo url_for('etapes') ?>">Inscription/Etapes</a>
              </li>
              <li>
                <a href="<?php echo url_for('reglement') ?>">Règlement</a>
              </li>
              <li>
                <a href="<?php echo url_for('@cms_page_emptyslug') ?>partenaires">Partenaires</a>
              </li>
              <li>
                <a href="<?php echo url_for('@cms_page_emptyslug') ?>faq">FAQ</a>
              </li>
              <li>
                <a href="<?php echo url_for('@contact') ?>">Contact</a>
              </li>
            </ul>
            <ul class="nav pull-right">
              <?php
              if ($sf_user->isAuthenticated()) {
                ?>
                <li class="dropdown ">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mon Compte
                    (<?php echo $sf_user->getUsername() ?>) <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo url_for('@mes_inscriptions') ?>">Mes inscriptions</a>
                    </li>
                    <li><a href="<?php echo url_for('@settings') ?>" title="<?php echo $sf_user->getUsername() ?>">Préférences</a>
                    </li>
                    <li><a href="<?php echo url_for('@sf_guard_signout') ?>">Déconnexion</a></li>
                  </ul>
                </li>
              <?php
              } else {
                ?>
                <li><a href="<?php echo url_for('@login') ?>">Connexion</a></li>
              <?php
              }
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="content">
    <div class="content-inside" style="padding-top: 10px;">
      <?php echo $sf_content ?>
      <div class="clear"></div>
    </div>
  </div>

  <div id="footer">
    <div class="content-inside">
      <div id="partenaires">
        <a href="http://www.supinfo.com/" class="partenaire">
          <?php echo image_tag('part_supinfo', array('alt' => 'SUPINFO')) ?>
        </a>
        <a href="http://www.yurplan.com/" class="partenaire">
          <?php echo image_tag('part_yurplan', array('alt' => 'YurPlan')) ?>
        </a>
        <a href="http://www.wanadev.fr/" class="partenaire">
          <?php echo image_tag('part_wanadev', array('alt' => 'Wanadev')) ?>
        </a>
        <a href="http://fr.norton.com/" class="partenaire">
          <?php echo image_tag('part_norton', array('alt' => 'Norton')) ?>
        </a>
        <a href="http://www.mtxserv.fr/" class="partenaire">
          <?php echo image_tag('part_mtxserv', array('alt' => 'mTxServ')) ?>
        </a>
        <div class="clear"></div>
      </div>
      <p style="margin-top:10px;padding:0;">
        &copy; 2012-2013 SNGT
        -
        SUPINFO National Gaming Tour
        -
        Tous droits réservés
        -
        Un évènement
        <a href="http://ubi-team.net" style="text-decoration: none; color: #444444;">UBITEAM</a>
        -
        Réalisation
        <a href="http://germain-carre.fr" style="text-decoration: none; color: #444444;">Germain Carré</a>
        -
        Design
        <a href="http://wanadev.fr" style="text-decoration: none; color: #444444;">Wanadev</a>
      </p>

      <p style="padding:0;">
        <small>
          Fièrement hébergé par
          <a href="http://wanadev.fr" style="text-decoration: none; color: #444444;">Wanadev</a> -
          Crédits photos :
          <a href="https://www.facebook.com/maximedophoto" style="text-decoration: none; color: #444444;">Maxime Do</a>
        </small>
      </p>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#header').click(function () {
    $(window).attr('location', '<?php echo url_for('@homepage') ?>');
  });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7726577-4']);
  _gaq.push(['_trackPageview']);

  (function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html> 
<div class="boxed scanqr" style="margin-top: 20px;">
  <p>
    <a href="zxing://scan/?ret=<?php echo urlencode(url_for('@mobile?username='.$username, true) . '?code={CODE}') ?>"
       class="btn btn-primary btn-larger btn-mobile">Cliquez ici pour Scanner</a>
  </p>
  <?php if ($code): ?>
    <p class="big" style="margin-top: 40px;">
      Code scanné: <?php echo $code ?><br />
      Le code à bien été envoyé.
    </p>
  <?php endif; ?>
  <p style="margin-top: 100px;">
    Pour scanner, vous avez besoin de
    <a href="https://itunes.apple.com/us/app/barcodes-scanner/id417257150?mt=8">Barcodes Scanner</a>
    pour Iphone ou
    <a href="https://play.google.com/store/apps/details?id=com.google.zxing.client.android&hl=fr">Barcode Scanner</a>
    pour Android
  </p>
</div>
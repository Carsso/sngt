<?php

/**
 * mobile actions.
 *
 * @package    SNGT
 * @subpackage mobile
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mobileActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->username = $request->getParameter('username', null);
    $user = Doctrine_Core::getTable('sfGuardUser')->findOneBy('username',$this->username);
    $this->forward404Unless($user);
    sfConfig::set('sf_web_debug', false);
    $this->code = $request->getParameter('code', null);
    if($this->code){
      $profile = $user->getProfile();
      $profile->setEntryCode($this->code)->save();
    }
  }
}

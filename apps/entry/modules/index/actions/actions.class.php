<?php

class indexActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $user = $this->getUser()->getGuardUser();
    $profile = $user->getProfile();
    $code = $profile->getEntryCode();
    if($code){
      $profile->setEntryCode(null)->save();
    }
  }
  public function executeUser(sfWebRequest $request)
  {
    $this->player = $this->getRoute()->getObject();
    $this->user = $this->player->getUser();
  }
  public function executeAjaxPlayer(sfWebRequest $request)
  {
    $player = $this->getRoute()->getObject();
    return $this->renderText($player->getId());
  }
  public function executeAjaxCode(sfWebRequest $request)
  {
    $user = $this->getUser()->getGuardUser();
    $profile = $user->getProfile();
    $code = $profile->getEntryCode();
    if($code){
      $profile->setEntryCode(null)->save();
    }
    return $this->renderText($code);
  }
}

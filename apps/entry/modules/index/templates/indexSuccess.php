<script type="text/javascript">
  var entry_ticket;
  var entry_ticket_val;
  var waiting_time;
  $(document).ready(function () {
    load();
    entry_ticket = $('#entry_ticket');
    entry_ticket.keyup(callAjaxChk);
    entry_ticket.change(callAjaxChk);
    setInterval(function () {
      updateAjxCode();
    }, 500);
  });
  function callAjaxChk() {
    clearTimeout(waiting_time);
    waiting_time = setTimeout(function () {
      updateAjxChk();
    }, 500);
  }
  function updateAjxChk() {
    entry_ticket.addClass('loading');
    entry_ticket_val = entry_ticket.val();
    $.ajax({
      url: "<?php echo url_for('ajax_entry_player_empty') ?>" + entry_ticket_val,
      success: function (data) {
        entry_ticket.css('background-color', '#daffe3');
        entry_ticket.removeClass('loading');
        $(location).attr('href','<?php echo url_for('entry_user_empty') ?>'+data)
      },
      error: function () {
        entry_ticket.css('background-color', '#ffc7ca');
        entry_ticket.removeClass('loading');
      }
    });
  }
  function updateAjxCode() {
    $.ajax({
      url: "<?php echo url_for('ajax_entry_code') ?>",
      success: function (data) {
        if (data != '') {
          entry_ticket.val(data);
          callAjaxChk();
        }
      }
    });
  }
</script>

<div class="boxed scanqr">
  <div style="margin-bottom: 10px;">
    <button onclick="setwebcam();$('#waitdiv').hide();$('#scandiv').hide();$('#mobilediv').hide();$('#loaddiv').show();"
            class="btn btn-primary btn-larger btn-webcam">Scanner via Webcam
    </button>
    <button onclick="$('#waitdiv').hide();$('#scandiv').hide();$('#mobilediv').show();$('#loaddiv').hide();"
            class="btn btn-primary btn-larger btn-webcam">Scanner via Mobile
    </button>
  </div>
  <div id="waitdiv">
    <p class="big" style="padding-top:80px;margin-bottom: 40px;">
      Choisissez le mode de scan pour commencer ou entrez ci-dessous le numéro du billet Yurplan
    </p>
  </div>
  <div id="loaddiv">
    <p style="padding-top:80px;margin-bottom: 40px;">
      Veuillez autoriser l'accès à votre caméra
    </p>

    <p>
      <img src="<?php echo image_path('sngt_load.gif') ?>"/>
    </p>
  </div>
  <div id="mobilediv">
    <p>
      Ouvrez l'URL suivante sur le navigateur de votre mobile:<br/>
      <a href="<?php echo url_for('@mobile?username=' . $sf_user->getUsername(), true) ?>"><?php echo url_for('@mobile?username=' . $sf_user->getUsername(), true) ?></a>
    </p>

    <p>
      Vous pouvez également scanner ce QR Code:<br/>
      <img
          src="http://chart.apis.google.com/chart?cht=qr&chs=200x200&chld=L&choe=UTF-8&chl=<?php echo urlencode(url_for('@mobile?username=' . $sf_user->getUsername(), true)) ?>"/>
    </p>
  </div>
  <div id="scandiv">
    <div id="outdiv">
      <video id="v" autoplay=""></video>
    </div>
    <div id="result"></div>
  </div>
  <canvas id="qr-canvas"></canvas>
  <form class="form-inline">
    <input type="text" id="entry_ticket"/>
  </form>
</div>





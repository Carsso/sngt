<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="title" content="SNGT - SUPINFO National Gaming Tour"/>
  <meta name="keywords"
        content="SUPINFO, LAN, jeux vidéos, joueurs, Starcraft, Lyon, League Of Legends, compétition, équipes, inscription, lots, récompenses, cashprice"/>
  <meta name="language" content="fr"/>
  <meta name="robots" content="index, follow"/>
  <title>SNGT - SUPINFO National Gaming Tour</title>
  <link rel="shortcut icon" href="http://sngt.fr/favicon.ico"/>
  <link rel="stylesheet" type="text/css" media="screen" href="http://sngt.fr/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="http://sngt.fr/css/design.css"/>
  <script type="text/javascript" src="http://sngt.fr/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="http://sngt.fr/js/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="http://sngt.fr/js/bootstrap.min.js"></script>
</head>
<body>

<div id="global">
  <div id="header" class="clear" style="cursor:pointer;">
    <div class="content-inside">
    </div>
  </div>

  <div class="navbar clear all_col column">
    <div class="navbar-inner" style="border-radius: 0">
      <div class="content-inside">
        <div class="container">
          <div class="nav-collapse">
            <ul class="nav">
              <li>
                <a href="http://sngt.fr">Retour à l'Accueil</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="content">
    <div class="content-inside">
      <div class="boxed" style="margin-top: 20px;min-height:550px;">
        <h2>Oups, Une erreur est survenue !</h2>

        <div style="margin-left:10px;">
          <p>Un problème technique est à l'origine de l'affichage de cette erreur. Si le problème persiste, merci de nous
            contacter.</p>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div id="footer">
    <p style="margin-top:10px;padding:0;">
      &copy; 2012-2013 SNGT - SUPINFO National Gaming Tour
      -
      Tous droits réservés
      -
      Un évènement
      <a href="http://ubi-team.net" style="text-decoration: none; color: #444444;">UBITEAM</a>
      -
      Réalisation :
      <a href="http://germain-carre.fr" style="text-decoration: none; color: #444444;">Germain Carré</a>
      -
      Design :
      <a href="http://wanadev.fr" style="text-decoration: none; color: #444444;">Wanadev</a>
    </p>

    <p style="padding:0;">
      <small>
        Fièrement hébergé par
        <a href="http://wanadev.fr" style="text-decoration: none; color: #444444;">Wanadev</a>
      </small>
    </p>
  </div>
</div>
<script type="text/javascript">
  $('#header').click(function () {
    $(window).attr('location', 'http://sngt.fr/');
  });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7726577-4']);
  _gaq.push(['_trackPageview']);

  (function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html> 
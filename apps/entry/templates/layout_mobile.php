<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <meta name="viewport" content="width=400" />
  <?php include_title() ?>
  <link rel="shortcut icon" href="<?php echo image_path('../favicon.ico') ?>"/>
  <?php include_stylesheets() ?>
  <?php include_javascripts() ?>
  <style type="text/css">
    .content-inside {
      width: 400px;
    }
  </style>
</head>
<body>
<div id="global">
  <?php
  if ($sf_user->hasFlash('info')) {
    echo '<div class="alert alert-info" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('info') . '</div>';
  }
  if ($sf_user->hasFlash('error')) {
    echo '<div class="alert alert-error" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('error') . '</div>';
  }
  if ($sf_user->hasFlash('success')) {
    echo '<div class="alert alert-success" style="margin: 5px 0 0"><a class="close" data-dismiss="alert" href="#">×</a>' . $sf_user->getFlash('success') . '</div>';
  }
  ?>
  <div id="header" class="clear" style="cursor:pointer;">
    <div class="content-inside" style="background-image: url('<?php echo image_path('sngtbanner_mini.png') ?>');height: 70px;">
    </div>
  </div>
  <div id="content">
    <div class="content-inside">
      <?php echo $sf_content ?>
      <div class="clear"></div>
    </div>
  </div>
  <script type="text/javascript">
    $('#header').click(function () {
      $(window).attr('location', '<?php echo url_for('@homepage') ?>');
    });
  </script>
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-7726577-4']);
    _gaq.push(['_trackPageview']);

    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();
  </script>
</body>
</html
<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actions
 *
 * @author david
 */
class pageCMSActions extends sfActions
{
  public function executePage(sfWebRequest $request)
  {
    $this->page = Doctrine::getTable('PageCms')->findOneBy('link', $request->getParameter('page'));
    if ($this->page) {
      $this->content = $this->page->getContent();
    }
  }
}
?>

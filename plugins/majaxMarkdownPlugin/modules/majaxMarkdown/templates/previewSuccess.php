<?php if ($sf_user->isAuthenticated() && $sf_user->getGuardUser()->getIsSuperAdmin()): ?>
  <?php echo majaxMarkdown::transform(sfOutputEscaper::unescape($preview), false, false) ?>
<?php else: ?>
  <?php echo majaxMarkdown::transform($preview, 'majaxMarkdown::userfilter', 'majaxMarkdown::userfilter2') ?>
<?php endif; ?>

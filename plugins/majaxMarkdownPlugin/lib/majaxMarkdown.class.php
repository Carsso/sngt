<?php
class majaxMarkdown
{
  public static function transform($string, $prerender_func = false, $postrender_func = false)
  {
    $path = dirname(__FILE__) . '/../vendor/' . sfConfig::get('app_majaxMarkdown_style', 'markdown_extra') . '/markdown.php';
    require_once($path);
    if ($prerender_func) {
      $render = $prerender_func;
      $string = call_user_func($render, $string);
    }
    $string = Markdown($string);
    if (sfConfig::get('app_majaxMarkdown_smartypants_enabled', true)) {
      $style = sfConfig::get('app_majaxMarkdown_smartypants_style', 'smartypants_typographer');
      $path = dirname(__FILE__) . '/../vendor/' . $style . '/smartypants.php';
      require_once($path);
      $string = SmartyPants($string, sfConfig::get('app_majaxMarkdown_smartypants_options', 1));
    }
    if (sfConfig::get('app_majaxMarkdown_post_render', false)) {
      $render = sfConfig::get('app_majaxMarkdown_post_render');
      $string = call_user_func($render, $string);
    }
    if ($postrender_func) {
      $render = $postrender_func;
      $string = call_user_func($render, $string);
    }
    return $string;
  }

  public static function userfilter($string)
  {
    $string = preg_replace('#(^|\n)&gt;#', '$1>', $string);
    return $string;
  }

  public static function userfilter2($string)
  {
    $string = preg_replace('#<code>(.+)</code>#isUe', '"<code>" . html_entity_decode(\'$1\') . "</code>"', $string);
    return $string;
  }
}

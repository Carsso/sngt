<?php use_helper('I18N') ?>
  Bonjour <?php echo $user->getFirstName() ?>,<br/><br/>

  Vous recevez cet email car vous avez indiqué avoir perdu votre mot de passe.<br/><br/>

  Vous pouvez changer votre mot de passe en cliquant sur le lien ci-dessous (actif 24h):<br/><br/>

<?php echo link_to('Cliquez ici pour changer votre mot de passe', '@sf_guard_forgot_password_change?unique_key=' . $forgot_password->unique_key, 'absolute=true') ?>
<?php use_helper('I18N') ?>
<div class="page-header">
  <h1><?php echo __('Hello %name%', array('%name%' => $user->getName()), 'sf_guard') ?></h1>
</div>

<h3><?php echo __('Enter your new password in the form below.', null, 'sf_guard') ?></h3>

<form
    action="<?php echo url_for('@sf_guard_forgot_password_change?unique_key=' . $sf_request->getParameter('unique_key')) ?>"
    method="post" class="form-horizontal">
  <fieldset>
    <div class="control-group">
      <?php echo $form['password']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['password']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['password_again']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password_again']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['password_again']->renderError() ?></span>
      </div>
    </div>
    <?php echo $form->renderHiddenFields(false) ?>
    <div class="control-group">
      <div class="controls">
        <button type="submit" class="btn btn-primary"><?php echo __('Change', null, 'sf_guard') ?></button>
      </div>
    </div>
  </fieldset>
</form>
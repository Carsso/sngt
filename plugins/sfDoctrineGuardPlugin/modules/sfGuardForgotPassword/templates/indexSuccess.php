<?php use_helper('I18N') ?>

<div class="boxed">
  <h2>Mot de passe oublié ?</h2>


  <div style="margin-left:10px;">
    <p>
      Remplissez le formulaire ci-dessous pour récupèrer votre mot de passe.
    </p>

    <form action="<?php echo url_for('@sf_guard_forgot_password') ?>" method="post" class="form-horizontal">
      <fieldset>
        <div class="control-group">
          <?php echo $form['email_address']->renderLabel(null, array('class' => 'control-label')) ?>
          <div class="controls">
            <?php echo $form['email_address']->render(array('required' => 'required', 'type' => 'email')) ?>
            <span class="help-inline"><?php echo $form['email_address']->renderError() ?></span>
          </div>
        </div>
        <?php echo $form->renderHiddenFields(false) ?>
        <div class="control-group">
          <div class="controls">
            <button type="submit" class="btn btn-primary">Envoyer</button>
            <a href="<?php echo url_for('@sf_guard_signin') ?>" class="btn">Annuler</a>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
</div>
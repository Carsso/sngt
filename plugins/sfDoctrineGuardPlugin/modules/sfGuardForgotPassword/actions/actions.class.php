<?php

require_once dirname(__FILE__) . '/../lib/BasesfGuardForgotPasswordActions.class.php';

/**
 * sfGuardForgotPassword actions.
 *
 * @package    sfGuardForgotPasswordPlugin
 * @subpackage sfGuardForgotPassword
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfGuardForgotPasswordActions extends BasesfGuardForgotPasswordActions
{

  public function executeIndex($request)
  {
    $this->form = new sfGuardRequestForgotPasswordForm();

    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid()) {
        $this->user = $this->form->user;
        $this->_deleteOldUserForgotPasswordRecords();

        $forgotPassword = new sfGuardForgotPassword();
        $forgotPassword->user_id = $this->form->user->id;
        $forgotPassword->unique_key = md5(rand() + time());
        $forgotPassword->expires_at = new Doctrine_Expression('NOW()');
        $forgotPassword->save();

        $message = Swift_Message::newInstance()
            ->setFrom(sfConfig::get('app_sf_guard_plugin_default_from_email', 'from@noreply.com'))
            ->setTo($this->form->user->email_address)
            ->setSubject('[SNGT] Demande de nouveau mot de passe pour ' . $this->form->user->username)
            ->setBody($this->getPartial('sfGuardForgotPassword/send_request', array('user' => $this->form->user, 'forgot_password' => $forgotPassword)))
            ->setContentType('text/html');

        $this->getMailer()->send($message);

        $this->getUser()->setFlash('success', 'Vérifiez vos emails, vous devriez recevoir quelquechose rapidement !');
        $this->redirect('@sf_guard_signin');
      } else {
        $this->getUser()->setFlash('error', 'Adresse email invalide, merci de réessayer !');
      }
    }
  }

  public function executeChange($request)
  {
    $this->forgotPassword = Doctrine_Core::getTable('sfGuardForgotPassword')->findOneBy('unique_key', $request->getParameter('unique_key', null));
    $this->forward404If(!is_object($this->forgotPassword), 'incorrect forgot password key');
    $this->user = $this->forgotPassword->User;
    $this->form = new sfGuardChangeUserPasswordForm($this->user);

    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid()) {
        $this->form->save();

        $this->_deleteOldUserForgotPasswordRecords();

        $message = Swift_Message::newInstance()
            ->setFrom(sfConfig::get('app_sf_guard_plugin_default_from_email', 'from@noreply.com'))
            ->setTo($this->user->email_address)
            ->setSubject('[SNGT] Nouveau mot de passe pour ' . $this->user->username)
            ->setBody($this->getPartial('sfGuardForgotPassword/new_password', array('user' => $this->user, 'password' => $request['sf_guard_user']['password'])));

        $this->getMailer()->send($message);

        $this->getUser()->setFlash('success', 'Mot de passe mis à jour !');
        $this->redirect('@sf_guard_signin');
      }
    }
  }

  protected function _deleteOldUserForgotPasswordRecords()
  {
    Doctrine_Core::getTable('sfGuardForgotPassword')
        ->createQuery('p')
        ->delete()
        ->where('p.user_id = ?', $this->user->id)
        ->execute();
  }
}

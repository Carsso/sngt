<?php use_helper('I18N') ?>
<div class="boxed">
  <h2>Inscription</h2>

  <div style="margin-left:10px;">

    <?php echo get_partial('sfGuardRegister/form', array('form' => $form)) ?>
  </div>
</div>
<?php use_helper('I18N') ?>

<form action="<?php echo url_for('@sf_guard_register') ?>" method="post" class="form-horizontal">
  <fieldset>
    <div class="control-group">
      <?php echo $form['first_name']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['first_name']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['first_name']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['last_name']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['last_name']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['last_name']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['username']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['username']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['username']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['email_address']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['email_address']->render(array('required' => 'required', 'type' => 'email')) ?>
        <span class="help-inline"><?php echo $form['email_address']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['password']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['password']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['password_again']->renderLabel(null, array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password_again']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['password_again']->renderError() ?></span>
      </div>
    </div>
    <?php echo $form->renderHiddenFields(false) ?>
    <div class="control-group">
      <div class="controls">
        <button type="submit" class="btn btn-primary">Inscription</button>
        <a href="<?php echo url_for('@sf_guard_signin') ?>" class="btn">Annuler</a>
      </div>
    </div>
  </fieldset>
</form>

<script type="text/javascript">
  $('#sf_guard_user_first_name').autotab_filter({ format: 'text', camelcase: true });
  $('#sf_guard_user_last_name').autotab_filter({ format: 'text', uppercase: true });
  $('#sf_guard_user_email_address').autotab_filter({ format: 'all', lowercase: true, nospace: true });
</script>
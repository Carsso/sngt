<?php use_helper('I18N') ?>
<div class="boxed">
  <h2>Connexion</h2>

  <div style="margin-left:10px;">
    <?php echo get_partial('sfGuardAuth/signin_form', array('form' => $form)) ?>
  </div>
</div>
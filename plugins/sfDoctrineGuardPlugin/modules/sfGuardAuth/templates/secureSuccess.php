<?php use_helper('I18N') ?>

<div class="boxed">
  <h2>Connexion</h2>

  <div style="margin-left:10px;">
    <p><?php echo sfContext::getInstance()->getRequest()->getUri() ?></p>

    <h3>Vous n'avez pas les permissions nécessaires pour accéder à cette page.</h3>

    <?php echo get_component('sfGuardAuth', 'signin_form') ?>
  </div>
</div>
<?php use_helper('I18N') ?>

<?php $routes = $sf_context->getRouting()->getRoutes() ?>
<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form-horizontal">
  <fieldset>
    <div class="control-group">
      <?php echo $form['username']->renderLabel('Pseudo / Email', array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['username']->render(array('required' => 'required')) ?>
        <span class="help-inline"><?php echo $form['username']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['password']->renderLabel('Mot de passe', array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['password']->render(array('required' => 'required')) ?>
        <?php if (isset($routes['sf_guard_forgot_password'])): ?>
          <a href="<?php echo url_for('@sf_guard_forgot_password') ?>" class="btn btn-danger btn-mini">Mot de passe
            oublié ?</a>
        <?php endif; ?>
        <span class="help-inline"><?php echo $form['password']->renderError() ?></span>
      </div>
    </div>
    <div class="control-group">
      <?php echo $form['remember']->renderLabel('Se souvenir de moi', array('class' => 'control-label')) ?>
      <div class="controls">
        <?php echo $form['remember'] ?>
        <span class="help-inline"><?php echo $form['remember']->renderError() ?></span>
      </div>
    </div>
    <?php echo $form->renderHiddenFields(false) ?>
    <div class="control-group">
      <div class="controls">
        <button type="submit" class="btn btn-primary">Connexion</button>
        <?php if (isset($routes['apply'])): ?>
          <a href="<?php echo url_for('@apply') ?>" class="btn btn-success">Inscription</a>
        <?php endif; ?>
      </div>
    </div>
  </fieldset>
</form>
<?php
/**
 * rocksUpload
 *
 * @package    nestplace
 * @subpackage karmaActivity
 * @author     WanaDev
 */
class axRocksUploadPluginActions extends sfActions
{
  public function executeUpload(sfWebRequest $request)
  {
    sfConfig::set('sf_escaping_strategy', false);
    $config = sfYaml::load(sfConfig::get('sf_config_dir') . '/config.yml');
    $pieces_path = sfConfig::get('sf_upload_dir') . '/' . $config['pieces_path'];
    $allowedExtensions = $config['cand_allowed_ext'];
    $sizeLimit = 1 * 1024 * 1024;
    $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
    $type = $request->getParameter('type');
    $file = $request->getParameter('qqfile');
    $extra_name = $request->getParameter('extra_name');
    $extra_slug = $request->getParameter('extra_slug');
    $pathinfo = pathinfo($file);
    $pers = $request->getParameter('pers');
    $uniqid = uniqid() . '-' . $pers;
    $filename = $type . $uniqid;
    if ($extra_slug) {
      $filename = $extra_slug . $uniqid;
    }
    $this->result = $uploader->handleUpload($pieces_path, false, $filename);
    if ($this->result['success'] == true) {
      $immoPiece = Doctrine_Query::create()
          ->from('ImmoTypepiece')
          ->where('piece_type = ?', $type)
          ->andWhere('piece_idpers = ?', $pers)
          ->fetchOne();
      if (is_object($immoPiece)) {
        $piece = $immoPiece;
      } else {
        $piece = new ImmoTypepiece();
      }
      $piece->setPieceSrc($filename . '.pdf')
          ->setPieceType($type)
          ->setPieceIdpers($pers);

      if ($extra_slug) {
        $piece->setPieceExtraSlug($extra_slug);
      }
      if ($extra_name) {
        $piece->setPieceExtraName($extra_name);
      }
      $piece->save();
    }
    $this->result = json_encode($this->result);
  }
}

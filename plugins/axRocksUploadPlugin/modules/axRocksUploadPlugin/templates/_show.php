<?php use_javascript('/axRocksUploadPlugin/js/fileuploader.js') ?>
<?php use_stylesheet('/axRocksUploadPlugin/css/fileuploader.css') ?>
<div class="uploader_box">
  <div id="file-uploader-<?php echo $uniq ?>">
    <noscript>
      <p>Merci d'activer javascript dans votre navigateur</p>
    </noscript>
  </div>
  <div class="clear"></div>
</div>
<script type="text/javascript">
  var uploader<?php echo $uniq ?> = new qq.FileUploader({
    // pass the dom node (ex. $(selector)[0] for jQuery users)
    element: $('#file-uploader-<?php echo $uniq ?>')[0],
    // path to server-side upload script
    action: '<?php echo url_for("@axRocksUpload")?>',
    multiple: false,
    maxConnections: 30,
    params: {
      type: '<?php echo $type ?>',
      <?php if(isset($extra_name)) { ?>
      extra_name: '<?php echo addcslashes($extra_name, '\'') ?>',
      <?php } ?>
      <?php if(isset($extra_slug)) { ?>
      extra_slug: '<?php echo $extra_slug ?>',
      <?php }  ?>
      pers: '<?php echo $pers ?>'
    },
    <?php if((isset($oncomplete))&&($oncomplete)) { ?>
    onComplete: function (id, fileName, responseJSON) {
      <?php echo $sf_data->getRaw('oncomplete') ?>
    },
    <?php } ?>
    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif', 'pdf'],
    sizeLimit: 1048576 // max size
  });
  <?php
    if($alreadypresent) {
  ?>
  $('#file-uploader-<?php echo $uniq ?>').children('.qq-uploader').children('.qq-upload-list').html('<li class=" qq-upload-success"><span class="qq-upload-file">Fichier présent</span><span class="qq-upload-tick"></span><span class="qq-upload-cross"></span></li>');
  <?php
    } elseif($needed) {
  ?>
  $('#file-uploader-<?php echo $uniq ?>').children('.qq-uploader').children('.qq-upload-list').html('<li class=" qq-upload-success"><span class="qq-upload-file">Fichier requis</span><span class="qq-upload-tick" style="display:none;"></span><span class="qq-upload-cross" style="display:inline-block;"></span></li>');
  <?php
    }
  ?>
</script>

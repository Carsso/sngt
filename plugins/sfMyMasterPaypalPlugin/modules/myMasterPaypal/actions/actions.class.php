<?php

/**
 * commande actions.
 *
 * @package    name
 * @subpackage commande
 * @author     Carsso
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class myMasterPaypalActions extends sfActions
{
  public function executeDone(sfWebRequest $request)
  {
    if ($request->hasParameter('custom') && $request->getParameter('custom', null)) {
      $context = sfContext::getInstance();
      $context->getConfiguration()->loadHelpers(array('Text', 'Url', 'Asset'));
      $player = Doctrine_Core::getTable('Player')->find($request->getParameter('custom', 0));
      $team = $player->getTeam();
      $round = $team->getRound();
      if (is_object($player) && $player->getId()) {
        $paypal = new sfMyMasterPaypal('20.00', 'Participation SNGT - Réservation: 20€');
      } else {
        $paypal = new sfMyMasterPaypal('20.00', 'Participation SNGT: 20€ | ' . truncate_text($player->__toString(), 25) . ' (' . truncate_text($team->__toString(), 25) . ') | ' . truncate_text($round->getGame()->getLittleName(), 20) . ' (' . truncate_text($round->getStep()->getCity(), 12) . ')');
      }
    } else {
      $paypal = new sfMyMasterPaypal('20.00', 'Participation SNGT - Réservation: 20€');
    }

    $result = $paypal->doPayment($request);
    if ($result) {
      $paypal->unsetParameters(array('PAYMENTACTION', 'PAYERID'));
      $details = $paypal->getPaymentDetails($request);
      $player = Doctrine::getTable('Player')->find($details['CUSTOM']);
      if (is_object($player)) {
        $payment = $player->getPayment();
        $payment->setTransaction($result['TRANSACTIONTYPE'] . ' - ' . $result['ORDERTIME'] . ' - ' . $result['AMT'] . ' - ' . $result['TRANSACTIONID'])
            ->setEmailPaypal($details['EMAIL'])
            ->setPayed(1);
        $payment = $payment->save();
        $player->getTeam()->setSlotReserved(1)->save();
        $this->redirect(url_for('participation_joueurs', $player->getTeam()) . '?payment=ok');
      }
    }
  }

  public function executeCancel(sfWebRequest $request)
  {
    $paypal = new sfMyMasterPaypal('20.00', 'Participation SNGT - Réservation: 20€');
    $details = $paypal->getPaymentDetails($request);
    $player = Doctrine::getTable('Player')->find($details['CUSTOM']);
    if (is_object($player)) {
      $this->redirect(url_for('participation_joueurs', $player->getTeam()) . '?payment=error');
    }
  }
}

<?php
class sfMyMasterPaypal
{
  private $parameters = array();
  private $server_url;
  private $api_url;

  public function setParameter($parameter, $value)
  {
    $this->parameters[$parameter] = $value;
  }

  public function getParameter($parameter)
  {
    return $this->parameters[$parameter];
  }

  public function setParameters($value = array())
  {
    $this->parameters = $value;
  }

  public function getParameters()
  {
    return $this->parameters;
  }

  public function setServerUrl($value)
  {
    $this->server_url = $value;
  }

  public function getServerUrl()
  {
    return $this->server_url;
  }

  public function setApiUrl($value)
  {
    $this->api_url = $value;
  }

  public function getApiUrl()
  {
    return $this->api_url;
  }

  public function unsetParameter($parameter)
  {
    unset($this->parameters[$parameter]);
  }

  public function unsetParameters($parameters)
  {
    foreach ($parameters as $parameter) {
      unset($this->parameters[$parameter]);
    }
  }

  public function __construct($amt, $description = '', $localecode = 'FR', $currency = 'EUR')
  {
    $context = sfContext::getInstance();
    $context->getConfiguration()->loadHelpers(array('Text', 'Url', 'Asset'));
    $request = $context->getRequest();
    $this->setParameter('VERSION', urlencode(sfConfig::get('app_my_master_paypal_version', '84.0')));
    $this->setParameter('USER', urlencode(sfConfig::get('app_my_master_paypal_user')));
    $this->setParameter('PWD', urlencode(sfConfig::get('app_my_master_paypal_pwd')));
    $this->setParameter('SIGNATURE', urlencode(sfConfig::get('app_my_master_paypal_signature')));
    $this->setParameter('CURRENCYCODE', urlencode($currency));
    $this->setParameter('DESC', urlencode($description));
    $this->setParameter('AMT', urlencode($amt));
    $this->setParameter('HDRIMG', urlencode(image_path(sfConfig::get('app_my_master_paypal_image', false), true)));
    $this->setServerUrl(sfConfig::get('app_my_master_paypal_url', 'https://www.paypal.com/webscr&cmd=_express-checkout&token='));
    $this->setApiUrl(sfConfig::get('app_my_master_paypal_api', 'https://api-3t.paypal.com/nvp?'));
  }

  public function expressCheckout()
  {
    $this->setParameter('CANCELURL', urlencode(url_for(sfConfig::get('app_my_master_paypal_cancel_route', '@myMasterPaypalCancel'), true) . '?custom=' . $this->getParameter('CUSTOM', '')));
    $this->setParameter('RETURNURL', urlencode(url_for(sfConfig::get('app_my_master_paypal_done_route', '@myMasterPaypalDone'), true) . '?custom=' . $this->getParameter('CUSTOM', '')));
    $this->setParameter('METHOD', 'SetExpressCheckout');
    $context = sfContext::getInstance();
    $context->getConfiguration()->loadHelpers('Url');
    $request = $context->getRequest();

    $site_base_url = $request->getUriPrefix();

    if (in_array("curl", get_loaded_extensions())) {
      $cmd = $this->api_url . $this->array_implode('=', '&', $this->getParameters());
      $ch = curl_init($cmd);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($ch);
      curl_close($ch);

      if (!$response) {
        print('No response from Paypal.');
      } else {
        $liste_param_paypal = $this->splitResult($response);
        if ($liste_param_paypal['ACK'] == 'Success') {
          header("Location: " . $this->server_url . $liste_param_paypal['TOKEN']);
          exit();
        } else {
          echo "<p>Une erreur PayPal est survenue:<br />" . $liste_param_paypal['L_SHORTMESSAGE0'] . "<br />" . $liste_param_paypal['L_LONGMESSAGE0'] . "</p>";
          die();
        }
      }
      return $response;
    } else {
      throw new sfException('Could not load cURL libraries. Make sure PHP is compiled with cURL.');
    }
  }

  public function doPayment($request)
  {
    $this->setParameter('METHOD', 'DoExpressCheckoutPayment');
    $this->setParameter('TOKEN', $request->getParameter('token'));
    $this->setParameter('PAYMENTACTION', 'sale');
    $this->setParameter('PAYERID', $request->getParameter('PayerID'));
    $context = sfContext::getInstance();
    $context->getConfiguration()->loadHelpers('Url');
    $request = $context->getRequest();

    $site_base_url = $request->getUriPrefix();

    if (in_array("curl", get_loaded_extensions())) {
      $cmd = $this->api_url . $this->array_implode('=', '&', $this->getParameters());
      $ch = curl_init($cmd);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($ch);
      curl_close($ch);

      if (!$response) {
        print('No response from Paypal.');
      } else {
        $liste_param_paypal = $this->splitResult($response);
        if ($liste_param_paypal['ACK'] == 'Success') {
          return $liste_param_paypal;
        } else {
          echo "<p>Une erreur PayPal est survenue:<br />" . $liste_param_paypal['L_SHORTMESSAGE0'] . "<br />" . $liste_param_paypal['L_LONGMESSAGE0'] . "</p>";
          die();
        }
      }
      return false;
    } else {
      throw new sfException('Could not load cURL libraries. Make sure PHP is compiled with cURL.');
    }
  }

  public function getPaymentDetails($request)
  {
    $this->setParameter('METHOD', 'GetExpressCheckoutDetails');
    $this->setParameter('TOKEN', $request->getParameter('token'));
    $context = sfContext::getInstance();
    $context->getConfiguration()->loadHelpers('Url');
    $request = $context->getRequest();

    $site_base_url = $request->getUriPrefix();

    if (in_array("curl", get_loaded_extensions())) {
      $cmd = $this->api_url . $this->array_implode('=', '&', $this->getParameters());
      $ch = curl_init($cmd);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($ch);
      curl_close($ch);

      if (!$response) {
        print('No response from Paypal.');
      } else {
        $liste_param_paypal = $this->splitResult($response);
        if ($liste_param_paypal['ACK'] == 'Success') {
          return $liste_param_paypal;
        } else {
          echo "<p>Une erreur PayPal est survenue:<br />" . $liste_param_paypal['L_SHORTMESSAGE0'] . "<br />" . $liste_param_paypal['L_LONGMESSAGE0'] . "</p>";
          die();
        }
      }
      return false;
    } else {
      throw new sfException('Could not load cURL libraries. Make sure PHP is compiled with cURL.');
    }
  }

  private function splitResult($resultat_paypal)
  {
    $liste_parametres = explode("&", $resultat_paypal);
    foreach ($liste_parametres as $param_paypal) {
      list($nom, $valeur) = explode("=", $param_paypal);
      $liste_param_paypal[$nom] = urldecode($valeur);
    }
    return $liste_param_paypal;
  }

  private function array_implode($glue, $separator, $array)
  {
    if (!is_array($array)) return $array;
    $string = array();
    foreach ($array as $key => $val) {
      if (is_array($val))
        $val = implode(',', $val);
      $string[] = "{$key}{$glue}{$val}";

    }
    return implode($separator, $string);
  }

}
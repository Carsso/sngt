<?php use_helper('I18N') ?>
<?php use_stylesheets_for_form($form) ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>
<div class="sf_apply sf_apply_reset boxed">
  <p>
    <?php echo __('You may change your password using the form below.', array(), 'sfForkedApply') ?>
  </p>

  <form method="post" action="<?php echo url_for("sfApply/reset") ?>" name="sf_apply_reset_form"
        id="sf_apply_reset_form" class="form-horizontal">
    <?php echo $form ?>
    <div class="form-actions actions sf_admin_actions">
      <input type="submit" value="<?php echo __("Reset My Password", array(), 'sfForkedApply') ?>"
             class="btn btn-primary">
      <?php echo link_to(__('Cancel'), 'sfApply/resetCancel', array('class' => 'btn')) ?>
    </div>
  </form>
</div>

<?php use_helper('I18N') ?>
<?php use_stylesheets_for_form($form) ?>
<?php
// Override the login slot so that we don't get a login prompt on the
// apply page, which is just odd-looking. 0.6
?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>
<div class="sf_apply sf_apply_apply boxed">
  <h2><?php echo __("Apply for an Account", array(), 'sfForkedApply') ?></h2>
  <?php include_partial('index/register_form', array('form' => $form)) ?>
</div>

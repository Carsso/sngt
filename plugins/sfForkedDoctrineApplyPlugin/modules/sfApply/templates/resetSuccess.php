<?php use_helper('I18N') ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>
<div class="sf_apply sf_apply_reset boxed">
  <form method="post" action="<?php echo url_for("sfApply/reset") ?>" name="sf_apply_reset_form"
        id="sf_apply_reset_form" class="form-horizontal">
    <p>
      <?php echo __('Thanks for confirming your email address. You may now change your
password using the form below.', array(), 'sfForkedApply') ?>
    </p>
    <?php echo $form ?>
    <div class="form-actions actions sf_admin_actions">
      <input type="submit" value="<?php echo __("Reset My Password", array(), 'sfForkedApply') ?>"
             class="btn btn-primary">
      <?php echo link_to(__('Cancel', array(), 'sfForkedApply'), 'sfApply/resetCancel', array('class' => 'btn')) ?>
    </div>
  </form>
</div>

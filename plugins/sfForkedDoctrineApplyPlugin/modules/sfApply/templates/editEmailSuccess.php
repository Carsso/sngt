<?php use_stylesheets_for_form($form) ?>
<?php use_helper("I18N") ?>
<div class="sf_apply sf_apply_settings boxed">
  <h2><?php echo __("Edit Email", array(), 'sfForkedApply') ?></h2>

  <form method="post" action="<?php echo url_for("sfApply/editEmail") ?>" name="sf_apply_email_edit_form"
        id="sf_apply_email_edit_form" class="form-horizontal">
    <?php echo $form ?>
    <div class="form-actions actions sf_admin_actions">
      <input type="submit" value="<?php echo __("Save", array(), 'sfForkedApply') ?>" class="btn btn-primary">
      <?php echo link_to(__("Cancel", array(), 'sfForkedApply'), sfConfig::get('app_sfForkedApply_after', sfConfig::get('app_sfForkedApply_after', '@settings')), array('class' => 'btn')) ?>
    </div>
  </form>
</div>

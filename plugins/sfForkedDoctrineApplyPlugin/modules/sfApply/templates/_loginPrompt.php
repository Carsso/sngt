<?php use_helper('I18N') ?>
<form method="post" action="<?php echo url_for("@sf_guard_signin") ?>" name="sf_guard_signin" id="sf_guard_signin"
      class="form-horizontal sf_apply_signin_inline">

  <?php echo $form ?>

  <div class="form-actions actions sf_admin_actions">
    <input type="submit" value="<?php echo __('sign in', array(), 'sfForkedApply') ?>" class="btn btn-primary">
    <?php echo link_to(__('Create a New Account', array(), 'sfForkedApply'), 'sfApply/apply', array('class' => 'btn btn-success')) ?>
    <?php echo link_to(__('Reset Your Password', array(), 'sfForkedApply'), 'sfApply/resetRequest', array('class' => 'btn btn-danger'))  ?>
  </div>
  <p>
  </p>

  <p>
  </p>
</form>

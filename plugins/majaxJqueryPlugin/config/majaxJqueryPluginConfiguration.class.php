<?php

/**
 * majaxJqueryPlugin configuration.
 *
 * @package     majaxJqueryPlugin
 * @subpackage  config
 * @author      Jacob Mather
 * @version     SVN: $Id: PluginConfiguration.class.php 17207 2009-04-10 15:36:26Z Kris.Wallsmith $
 */
class majaxJqueryPluginConfiguration extends sfPluginConfiguration
{
  const VERSION = '1.0.0-DEV';

  /**
   * @see sfPluginConfiguration
   */
  public function initialize()
  {
    $helpers = sfConfig::get('sf_standard_helpers', array());
    $helpers[] = 'MajaxjQuery';
    sfConfig::set('sf_standard_helpers', $helpers);
  }
}

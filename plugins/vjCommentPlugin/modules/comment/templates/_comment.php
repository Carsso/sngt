<div class="comment<?php if ($obj->is_delete) echo " deleted"; ?> bordered rounded"
     style='margin-top: 5px; padding: 5px;'>
  <?php include_partial("comment/comment_author", array('obj' => $obj, 'i' => $i, 'website' => $obj->getWebsite(), 'name' => $obj->getAuthor())) ?>
  <?php if ($isAdmin) { ?>
    <div class="suppr_comment">
      <?php echo link_to(image_tag('delete.png', array('style' => 'margin:5px', 'alt' => 'Delete')), '@comment_delete?id=' . $obj->getId(), array('method' => 'delete', 'confirm' => 'Êtes-vous sûr(e) ?', 'style' => 'background-position:0 80px;border-color:#d33937')) ?>
    </div>
  <?php } ?>
  <?php include_partial("comment/comment_body", array('obj' => $obj)) ?>
  <?php include_partial("comment/comment_infos", array('obj' => $obj, 'form_name' => $form_name, 'date' => $obj->getCreatedAt())) ?>
</div>
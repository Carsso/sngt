<?php if (!$obj->is_delete): ?>
  <div class="body_comment" id="body_<?php echo $obj->id ?>"><?php echo $obj->getBody(ESC_RAW) ?></div>
<?php else: ?>
  <div class="msg-deleted"><?php echo __('Comment deleted by moderator', array(), 'vjComment') ?></div>
<?php endif ?>
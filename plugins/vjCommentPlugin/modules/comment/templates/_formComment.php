<?php use_helper('I18N', 'JavascriptBase') ?>
<a name="comments-<?php echo $crypt ?>"></a>
<div class="form-comment">
  <?php if (vjComment::checkAccessToForm($sf_user)): ?>
    <form action="<?php echo url_for($sf_request->getUri()) ?>" method="post">
      <h4><?php echo __('Add new comment', array(), 'vjComment') ?></h4>

      <p>
        <?php echo $form['body']->renderLabel(null, array('class' => 'hidden')) ?> <?php echo $form['body'] ?>
        <?php echo $form->renderHiddenFields() ?>
        <input type="submit" value="<?php echo __('send', array(), 'vjComment') ?>" class="designedbutton submit"
               style="float:right;height:42px; margin: 35px"/>
      </p>
    </form>
  <?php else: ?>
    <div id="notlogged"><h4><?php echo __('Please log in to comment', array(), 'vjComment') ?></h4></div>
  <?php endif ?>
</div>
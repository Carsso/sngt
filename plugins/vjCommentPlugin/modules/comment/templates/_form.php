<?php foreach ($form as $id => $f): ?>
  <?php if ($id == "reply_author" && $f->getValue() != ""): ?>
  <?php endif ?>
  <?php if (!$f->isHidden()): ?>
    <?php if (!$f->hasError()): ?>
      <?php $attributes = array() ?>
    <?php else: ?>
      <?php $attributes = array('class' => 'error') ?>
      <?php echo $f->renderError() ?>

    <?php endif ?>
    <?php echo $f->renderLabel(null, $attributes) ?>
    <?php echo $f->render($attributes) ?>
    <span class="help"><?php echo $f->renderHelp() ?></span>
    <?php if ($id == "reply_author"): ?>
      <?php echo link_to_function(__("Delete the reply", array(), 'vjComment'), "deleteReply('" . $form->getName() . "')", array('class' => 'delete_reply')) . "\n" ?>
    <?php endif ?>
  <?php endif ?>
<?php endforeach ?>
<?php echo $form->renderHiddenFields() ?>
<input type="submit" value="<?php echo __('send', array(), 'vjComment') ?>" class="submit"/>
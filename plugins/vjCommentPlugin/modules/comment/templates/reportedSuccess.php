<?php use_helper('JavascriptBase', 'I18N') ?>
<div id="report-sent">
  <span>
        <h4><?php echo __('Report sent.', array(), 'vjComment') ?></h4>
    <?php echo __('The moderation team has been notified.', array(), 'vjComment') ?>
  </span><br/>
  <br/>
  <?php echo link_to_function(__('Close the popup', array(), 'vjComment'), 'window.close()', array('class' => 'designedbutton')) ?>
</div>
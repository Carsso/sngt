<?php if ($has_comments): ?>
  <?php use_helper('Date', 'JavascriptBase', 'I18N') ?>
  <?php if (commentTools::isGravatarAvailable()): ?>
    <?php use_helper('Gravatar') ?>
  <?php endif ?>
  <div>
    <h4><?php echo __('Comments list', array(), 'vjComment') ?> (<?php echo $pager->getNbResults() ?>)</h4>
  </div>
  <?php if ($pager->haveToPaginate()): ?>
    <?php include_partial('comment/pagination', array('pager' => $pager, 'route' => $sf_request->getUri(), 'crypt' => $crypt, 'position' => 'top')) ?>
  <?php endif ?>
  <?php foreach ($pager->getResults() as $c): ?>
    <?php include_partial("comment/comment", array('obj' => $c, 'i' => (++$i + $cpt), 'first_line' => ($i == 1), 'form_name' => $form_name, 'estate' => $object, 'isAdmin' => $isAdmin)) ?>
  <?php endforeach; ?>
  <?php if ($pager->haveToPaginate()): ?>
    <?php include_partial('comment/pagination', array('pager' => $pager, 'route' => $sf_request->getUri(), 'crypt' => $crypt, 'position' => 'back')) ?>
  <?php endif ?>
<?php else: ?>
  <h4><?php echo __('Be the first to post', array(), 'vjComment') ?></h4>
<?php endif ?>
<?php use_helper('I18N') ?>
<div class="form-comment">
  <form action="" method="post" id="reportComment">
    <h4><?php echo __('Report a comment', array(), 'vjComment') ?></h4>

    <p>
      <?php echo $form['reason']->renderLabel(null, array('class' => 'hidden')) ?> <?php echo $form['reason'] ?>
    </p>

    <p>
      <?php echo $form->renderHiddenFields() ?>
      <input type="submit" value="<?php echo __('send', array(), 'vjComment') ?>" class="designedbutton submit"/>
    </p>
  </form>
</div>